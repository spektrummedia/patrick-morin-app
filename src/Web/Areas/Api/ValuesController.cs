﻿using Microsoft.AspNetCore.Mvc;

namespace PatrickMorin.Web.Areas.Api
{
    [Area(AreaNames.Api)]
    public class ValuesController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return Json(new[] {"value1", "value2"});
        }
    }
}