﻿using System;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Services;
using PatrickMorin.Web.Models;
using Spk.Common.Helpers.String;
using Index = PatrickMorin.Web.Resources.Pages.RemotePayOrder.Index;

namespace PatrickMorin.Web.Pages.RemotePayOrder
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IOrderService _orderService;
        private readonly IOgasysService _ogasysService;
        private readonly ResourceManager _res;

        [BindProperty]
        public string EPaymentId { get; set; }

        [BindProperty]
        public CardModel Card { get; set; }

        public RemotePayOrderDto Order { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IOgasysService ogasysService, IOrderService orderService)
        {
            _logger = logger;
            _orderService = orderService;
            _ogasysService = ogasysService;
            _res = new ResourceManager(Contants.ERRORS_RESOURCE_FILE_PATH, typeof(OgasysService).Assembly);
            Card = new CardModel();
        }

        public async Task<ActionResult> OnGetAsync(string id)
        {
            if (id.IsNullOrWhiteSpace())
                return RedirectToPage("NotFound");

            var remotePayOrder = await _ogasysService.GetRemotePayOrder(id);
            if (remotePayOrder?.Order == null)
                return RedirectToPage("NotFound");

            if (remotePayOrder.Order?.Items?.Item == null || !remotePayOrder.Order.Items.Item.Any() ||
                remotePayOrder.Amount == null)
            {
                _logger.LogWarning("Ogasys order was not correctly built. There's no amount or no items.");
                return RedirectToPage("NotFound");
            }

            Order = remotePayOrder;
            EPaymentId = id;

            return Page();
        }

        public async Task<ActionResult> OnPostAsync()
        {
            if (EPaymentId.IsNullOrWhiteSpace())
            {
                _logger.LogError("You must set ePaymentId.");
                return RedirectToPage("NotFound");
            }

            var remotePayOrder = await _ogasysService.GetRemotePayOrder(EPaymentId);
            if (remotePayOrder?.Order == null || remotePayOrder.Amount == null)
            {
                _logger.LogError("Could not find remote pay order in Ogasys.");
                return RedirectToPage("NotFound");
            }
            Order = remotePayOrder;

            ValidateExpirationDate();
            if (!ModelState.IsValid)
                return Page();

            if (remotePayOrder.Amount.Balance >= 0)
            {
                var paymentResult = await _orderService.Payment(Card.MapToDto(), Order);
                if (!paymentResult.Success)
                {
                    TempData["PaymentSuccess"] = false;
                    TempData["PaymentMessage"] = !paymentResult.Message.IsNullOrEmpty() ? paymentResult.Message :
                        _res.GetString("ErrorOccuredDuringPayment");
                }
                else
                {
                    TempData["PaymentSuccess"] = true;
                    Order = paymentResult.Order;
                }
            }

            return Page();
        }

        public void ValidateExpirationDate()
        {
            if (Card.ExpiryYear == 0 || Card.ExpiryMonth == 0)
                return;

            var daysInMonth = DateTime.DaysInMonth(Card.ExpiryYear, Card.ExpiryMonth);
            var expirationDate = new DateTime(Card.ExpiryYear, Card.ExpiryMonth, daysInMonth);
            if (DateTime.Today > expirationDate)
                ModelState.AddModelError("Card.ExpiryDate", Index.InvalidDateErrorMessage);

            Card.ExpiryDate = Card.ExpiryMonth + "/" + Card.ExpiryYear;
        }
    }
}
