﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PatrickMorin.Application.Services;

namespace PatrickMorin.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IOgasysService _ogasysService;

        [BindProperty]
        public string OrderNo { get; set; }

        public IndexModel(IOgasysService ogasysService)
        {
            _ogasysService = ogasysService;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            var remotePayOrder = await _ogasysService.GetRemotePayOrderByOrderNo(OrderNo);
            if (remotePayOrder == null)
                return RedirectToPage("NotFound");

            return RedirectToPage("./RemotePayOrder/Index", new { id = remotePayOrder.EPaymentId });
        }
    }
}
