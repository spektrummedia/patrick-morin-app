using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Extensions;
using PatrickMorin.Application.Services;
using PatrickMorin.Web.Areas;
using PatrickMorin.Web.Models;
using Spk.Common.Helpers.String;

namespace PatrickMorin.Web.Controllers
{
    [Area(AreaNames.Api)]
    public class RemotePayOrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IOgasysService _ogasysService;
        private readonly INotificationService _notificationService;
        private readonly ILogger<RemotePayOrderController> _logger;

        public RemotePayOrderController(ILogger<RemotePayOrderController> logger, IOgasysService ogasysService, 
            INotificationService notificationService, IOrderService orderService)
        {
            _logger = logger;
            _orderService = orderService;
            _ogasysService = ogasysService;
            _notificationService = notificationService;
        }

        [HttpPost]
        public async Task<IActionResult> Notify([FromBody]RemotePayOrderModel model)
        {
            _logger.LogInformation($"Notify started for order with ePaymentId {model.EPaymentId}.");
            
            if (model.EPaymentId.IsNullOrWhiteSpace())
                return BadRequest("You must set a ePaymentId.");

            var order = await _ogasysService.GetRemotePayOrder(model.EPaymentId);
            if (order?.Contact == null)
            {
                _logger.LogWarning($"Notify - Order or contact is null for order {model.EPaymentId}.");
                return NotFound($"Order or contact is null for order {model.EPaymentId}.");
            }

            if (order.Contact.Type != ContactTypes.EMAIL && order.Contact.Type != ContactTypes.PHONE) {
                _logger.LogWarning($"Notify - Contact type {order.Contact.Type} is not valid.");
                return BadRequest($"Contact type {order.Contact.Type} is not valid.");
            }

            if (order.Contact.Number.IsNullOrWhiteSpace())
            {
                _logger.LogWarning($"Notify - Could not reach customer for order {model.EPaymentId} no phone or email is set.");
                return BadRequest($"Could not reach customer for order {model.EPaymentId} no phone or email is set.");
            }

            var contact = order.Contact;
            var orderNo = order.Order?.Header?.OrderNo;
            var orderUrl = Request.BaseUrl() + $"/remotepayorder/{order.EPaymentId}";

            var success = await _notificationService.NotifyOrderReceived(contact.Type, contact.Number, orderNo, orderUrl);
            if (!success)
            {
                _logger.LogError($"Error occured while sending notification to {contact.Number}.");
                return StatusCode(500, $"Error occured while sending notification to {contact.Number}.");
            }

            _logger.LogInformation($"Notify completed for order with ePaymentId {model.EPaymentId}.");
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Refund()
        {
            _logger.LogInformation("Refund started.");
            
            var body = await ReadRequestBody();
            _logger.LogInformation("Request body : {body}", body);
            
            var model = JsonConvert.DeserializeObject<RefundModel>(body);
            if (model?.Refunds == null || model.Order?.Header?.OrderNo == null)
            {
                _logger.LogWarning("Could not do refund because model, refund list or order_no was null.");
                return BadRequest("Model, refund list or order_no cannot be null.");
            }
            
            _logger.LogInformation("Order number : {orderNo}", model.Order.Header.OrderNo);
            if (model.Contact == null)
            {
                _logger.LogWarning($"Refund - Contact is null for order {model.EPaymentId}.");
                return NotFound($"Contact is null for order {model.EPaymentId}.");
            }

            var contact = model.Contact;
            if (contact.Type != ContactTypes.EMAIL && contact.Type != ContactTypes.PHONE)
            {
                _logger.LogWarning($"Refund - Contact type {contact.Type} is not valid.");
                return BadRequest($"Contact type {contact.Type} is not valid.");
            }

            if (contact.Number.IsNullOrWhiteSpace())
            {
                _logger.LogWarning($"Refund - Could not reach customer for order {model.EPaymentId} " +
                                   "no phone or email is set.");
                return BadRequest($"Could not reach customer for order {model.EPaymentId} no phone or email is set.");
            }

            var refundResult = await _orderService.Refund(
                model.Refunds.Select(x => x.MapToDto()).ToList(),
                contact,
                model.Order.Header.OrderNo);
            
            _logger.LogInformation("Refund result : {result}", JsonConvert.SerializeObject(refundResult));
            _logger.LogInformation("Refund completed for order {orderNo}.", model.Order.Header.OrderNo);

            return Ok(refundResult);
        }

        private async Task<string> ReadRequestBody()
        {
            using var reader = new StreamReader(Request.Body, Encoding.UTF8);
            var bodyStr = await reader.ReadToEndAsync();
            return bodyStr;
        }
    }
}
