﻿using System.ComponentModel.DataAnnotations;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Web.Resources.Pages.RemotePayOrder;

namespace PatrickMorin.Web.Models
{
    public class CardModel
    {
        [CreditCard(ErrorMessageResourceName = nameof(Index.CardErrorMessage), ErrorMessageResourceType = typeof(Index))]
        [Required(ErrorMessageResourceName = nameof(Index.RequiredErrorMessage), ErrorMessageResourceType = typeof(Index))]
        public string CardNumber { get; set; }

        [Required(ErrorMessageResourceName = nameof(Index.RequiredErrorMessage), ErrorMessageResourceType = typeof(Index))]
        public string CardholderName { get; set; }

        [Range(1, 12, ErrorMessageResourceName = nameof(Index.InvalidMonthErrorMessage), ErrorMessageResourceType = typeof(Index))]
        public int ExpiryMonth { get; set; }

        [Range(1, 9999, ErrorMessageResourceName = nameof(Index.InvalidYearErrorMessage), ErrorMessageResourceType = typeof(Index))]
        public int ExpiryYear { get; set; }

        [Required(ErrorMessageResourceName = nameof(Index.RequiredErrorMessage), ErrorMessageResourceType = typeof(Index))]
        public string SecurityCode { get; set; }

        public string ExpiryDate { get; set; }

        public CardDto MapToDto()
        {
            return new()
            {
                CardNumber = CardNumber,
                CardholderName = CardholderName,
                ExpiryYear = ExpiryYear,
                ExpiryMonth = ExpiryMonth
            };
        }
    }
}
