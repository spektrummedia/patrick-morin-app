﻿using System.Collections.Generic;
using PatrickMorin.Application.Dtos.Ogasys;

namespace PatrickMorin.Web.Models
{
    public class RefundModel
    {
        public string EPaymentId { get; set; }
        public ContactDto Contact { get; set; }
        public List<RefundItemModel> Refunds { get; set; }
        public OrderDto Order { get; set; }
    }
}
