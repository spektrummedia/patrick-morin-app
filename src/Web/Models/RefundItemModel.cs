﻿using PatrickMorin.Application.Dtos.Ogasys;

namespace PatrickMorin.Web.Models
{
    public class RefundItemModel
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string Reference { get; set; }
        public string ReceiptNumber { get; set; }

        public RefundItemDto MapToDto()
        {
            return new()
            {
                Id = Id,
                Amount = Amount,
                Reference = Reference,
                ReceiptNumber = ReceiptNumber
            };
        }
    }
}