﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PatrickMorin.Application;
using PatrickMorin.Application.Configuration;
using PatrickMorin.Application.Services;
using PatrickMorin.Domain;
using PatrickMorin.Domain.Entities;
using PatrickMorin.Persistence;

namespace PatrickMorin.Web.App_Startup
{
    public class Startup
    {
        public Startup(
            IConfiguration configuration,
            IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment HostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            MailingServices.Configure(services, HostingEnvironment, Configuration.GetSection("Mailing"));
            services.Configure<TwilioSettings>(Configuration.GetSection("Twilio"));
            services.Configure<OgasysSettings>(Configuration.GetSection("Ogasys"));
            services.Configure<GlobalPaymentSettings>(Configuration.GetSection("GlobalPayments"));

            services.AddTransient<DatabaseMigrator>();
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDbContext<PatrickMorinDbContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection"),
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(DesignTimeDbContextFactory).Assembly.FullName)));

            services.AddIdentity<User, Role>(options =>
                {
                    options.SignIn.RequireConfirmedEmail = true;
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequiredLength = 1;
                })
                .AddEntityFrameworkStores<PatrickMorinDbContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOgasysService, OgasysService>();
            services.AddScoped<ITwilioService, TwilioService>();
            services.AddScoped<IGlobalPaymentTransactionService, GlobalPaymentTransactionService>();
            services.AddScoped<IGlobalPaymentService, GlobalPaymentService>();
            services.AddScoped<IHttpRequestService, HttpRequestService>();
            services.AddScoped<INotificationService, NotificationService>();

            services.AddCors();
            services.AddRazorPages()
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AuthorizeFolder("/Account/Manage");
                    options.Conventions.AuthorizePage("/Account/Logout");
                })
                .AddViewLocalization(opts => { opts.ResourcesPath = "Resources"; });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseHttpsRedirection();

            if (!HostingEnvironment.IsProduction())
                app.Map("/.dev", dev => DevPipeline.Configure(dev, HostingEnvironment));

            app.Map("/api", api => ApiPipeline.Configure(api, HostingEnvironment));

            AppPipeline.Configure(app, HostingEnvironment);
        }
    }
}