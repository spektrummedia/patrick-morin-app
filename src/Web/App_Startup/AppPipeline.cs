﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PatrickMorin.Application;
using Twilio;

namespace PatrickMorin.Web.App_Startup
{
    internal static class AppPipeline
    {
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment hostingEnvironment)
        {
            if (hostingEnvironment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404)
                {
                    context.Request.Path = "/NotFound";
                    await next();
                }
            });

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapDefaultControllerRoute();
            });

            var configuration = app.ApplicationServices.GetService<IConfiguration>();
            var accountSid = configuration.GetSection("Twilio:AccountSid").Value;
            var authToken = configuration.GetSection("Twilio:AuthToken").Value;

            TwilioClient.Init(accountSid, authToken);

            var serviceScopeFactory = app.ApplicationServices.GetService<IServiceScopeFactory>();
            using var scope = serviceScopeFactory.CreateScope();
            IServiceProvider serviceProvider = scope.ServiceProvider;

            // Migrate database
            var migrator = serviceProvider.GetRequiredService<DatabaseMigrator>();
            migrator.Execute();
        }
    }
}