﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using PatrickMorin.Web.Areas;

namespace PatrickMorin.Web.App_Startup
{
    internal static class ApiPipeline
    {
        public static void Configure(IApplicationBuilder api, IWebHostEnvironment hostingEnvironment)
        {
            api.UseCors(hostingEnvironment.EnvironmentName);
            api.UseRouting();
            api.UseEndpoints(endpoints =>
            {
                endpoints.MapAreaControllerRoute("api_route", AreaNames.Api, "{controller=Home}/{action=Index}");
            });
        }
    }
}