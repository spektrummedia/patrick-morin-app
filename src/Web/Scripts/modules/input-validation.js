export const Validation = {
	element: $('.js-form'),
	inputs: $('.js-form .js-required'),
	submitBtn: $('.js-submit-button'),
	init: function () {
		Validation.checkIfAllInputsAreSet();
		if (Validation.element) {
			Validation.setListeners();
		}
	},
	splitDate: function ($domobj, value) {
		if (!value) {
			$domobj.val(value);
			$domobj.attr("value", value);
			$domobj.siblings('input[data-name$="expiry-month"]').attr("value", '');
			$domobj.siblings('input[data-name$="expiry-year"]').attr("value", '');
		}
		var regExp = /(1[0-2]|0[1-9]|\d)\/(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)/;
		var matches = regExp.exec(value);
		if (matches) {
			$domobj.siblings('input[data-name$="expiry-month"]').attr("value", matches[1]);
			$domobj.siblings('input[data-name$="expiry-year"]').attr("value", '20' + matches[2]);
			$domobj.val(matches[1] + '/' + matches[2]);
			$domobj.attr("value", matches[1] + '/' + matches[2]);
		} else {
			var monthRegExp = /(1[0-2]|0[1-9]|\d)\//;
			var monthMatches = monthRegExp.exec(value);
			if (monthMatches && monthMatches.length > 0) {
				$domobj.siblings('input[data-name$="expiry-month"]').attr("value", monthMatches[1]);
			}
		}
	},
	expiryMask: function ($domobj, value) {
		var code = event.keyCode;
		var allowedKeys = [8];
		if (allowedKeys.indexOf(code) !== -1) {
			return;
		}

		event.target.value = event.target.value.replace(
			/^([1-9]\/|[2-9])$/g, '0$1/'
		).replace(
			/^(0[1-9]|1[0-2])$/g, '$1/'
		).replace(
			/^([0-1])([3-9])$/g, '0$1/$2'
		).replace(
			/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2'
		).replace(
			/^([0]+)\/|[0]+$/g, '0'
		).replace(
			/[^\d\/]|^[\/]*$/g, ''
		).replace(
			/\/\//g, '/'
		);
		Validation.splitDate($domobj, value);
		Validation.checkIfAllInputsAreSet();
	},
	setListeners: function() {
		$('input[data-name$="expiry-date"]').on('input', function() {
			Validation.expiryMask($(this), $(this).val());
		});
		for (var i = 0; i < Validation.inputs.length; ++i) {
			Validation.inputs[i].addEventListener('keyup', Validation.checkIfAllInputsAreSet);
		}
		$('.js-form').on('submit', function () {
			Validation.submitBtn.disabled = true;
			Validation.submitBtn.value = "Envoi en cours...";
			return true;
		});
	},
	checkIfAllInputsAreSet: function () {
		var oneInputIsEmpty = Array.from(Validation.inputs).some(input => !input.value || input.value === "0");
		if (!Validation.submitBtn)
			return;
		if (!oneInputIsEmpty) {
			Validation.submitBtn.prop('disabled', false);
		} else {
			Validation.submitBtn.prop('disabled', true);
		}
	}
};