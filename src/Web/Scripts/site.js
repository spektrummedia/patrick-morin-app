import 'babel-polyfill';
import { Validation as validation } from './modules/input-validation'

(() => {
	setTimeout(() => {
		validation.init();
	}, 50);
})();