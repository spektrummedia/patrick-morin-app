﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using PatrickMorin.Web.App_Startup;

namespace PatrickMorin.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("****************************************");
            Console.WriteLine($"dotnet.exe process id: {Process.GetCurrentProcess().Id}");
            Console.WriteLine("****************************************");
            BuildWebHost(args).Run();
        }

        public static IHost BuildWebHost(string[] args) => CreateWebHostBuilder(args).Build();


        public static IHostBuilder CreateWebHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                    webBuilder
                        .ConfigureAppConfiguration(config => config.AddJsonFile("appsettings.local.json", true))
                        .ConfigureLogging((ctx, logging) =>
                        {
                            logging.AddFilter<ConsoleLoggerProvider>(level =>
                                level == LogLevel.None);
                            Logging.Configure(logging, ctx.Configuration);
                        })
                        .UseUrls("https://patrickmorin.local:5000")
                        .UseStartup<Startup>()
                    );
    }
}
