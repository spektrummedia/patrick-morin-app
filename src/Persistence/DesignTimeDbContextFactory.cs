﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using PatrickMorin.Domain;

namespace PatrickMorin.Persistence
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<PatrickMorinDbContext>
    {
        public PatrickMorinDbContext CreateDbContext(string[] args)
        {
            var options = new DbContextOptionsBuilder<PatrickMorinDbContext>()
                .UseSqlServer(
                    @"Data Source=localhost\SQLEXPRESS;Initial Catalog=PatrickMorin;Integrated Security=True;", 
                    optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(DesignTimeDbContextFactory).Assembly.FullName))
                .Options;

            return new PatrickMorinDbContext(options);
        }
    }
}