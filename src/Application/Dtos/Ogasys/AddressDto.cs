﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class AddressDto
    {
        public string Address1 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
    }
}
