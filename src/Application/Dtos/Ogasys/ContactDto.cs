﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class ContactDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Number { get; set; }
    }
}
