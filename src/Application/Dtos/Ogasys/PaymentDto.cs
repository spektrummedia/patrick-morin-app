﻿using System;
using Newtonsoft.Json;

namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class PaymentDto
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("processor")]
        public string Processor { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("receiptnumber")]
        public string ReceiptNumber { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("card_type")]
        public string CardType { get; set; }

        [JsonProperty("cardno_mask")]
        public string CardNoMask { get; set; }

        [JsonProperty("cardno_fid")]
        public string CardNoFid { get; set; }

        [JsonProperty("pmt_mode")]
        public string PmtMode { get; set; }
    }
}
