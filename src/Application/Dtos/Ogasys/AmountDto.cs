﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class AmountDto
    {
        public decimal Initial { get; set; }
        public decimal Payment { get; set; }
        public decimal Balance { get; set; }
    }
}
