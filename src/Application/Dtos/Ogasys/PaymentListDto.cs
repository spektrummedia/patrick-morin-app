﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class PaymentListDto
    {
        [JsonProperty("payment")]
        public List<PaymentDto> Payment { get; set; }
    }
}
