﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class OrderDto
    {
        public HeaderDto Header { get; set; }
        public OrderItemsDto Items { get; set; }
    }
}
