﻿using Newtonsoft.Json;

namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class OrderItemDto
    {
        [JsonProperty("product_desc")]
        public string ProductDesc { get; set; }

        [JsonProperty("qty")]
        public decimal Quantity { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("discount_amount")]
        public decimal DiscountAmount { get; set; }

        [JsonProperty("discount_pct")]
        public decimal DiscountPercentage { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }
}
