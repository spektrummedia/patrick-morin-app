﻿using System.Collections.Generic;

namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class OrderItemsDto
    {
        public IList<OrderItemDto> Item { get; set; }
    }
}
