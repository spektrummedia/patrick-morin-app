﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class PaymentResultDto
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public RemotePayOrderDto Order { get; set; } = new();
    }
}
