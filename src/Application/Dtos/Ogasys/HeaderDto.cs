﻿using System;
using Newtonsoft.Json;

namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class HeaderDto
    {
        [JsonProperty("order_id")]
        public string OrderId { get; set; }

        [JsonProperty("order_no")]
        public string OrderNo { get; set; }

        [JsonProperty("order_date")]
        public DateTime OrderDate { get; set; }

        [JsonProperty("entrCode")]
        public string EntrCode { get; set; }

        [JsonProperty("entrName")]
        public string EntrName { get; set; }

        [JsonProperty("sub_total")]
        public decimal SubTotal { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }

        [JsonProperty("ship_custno")]
        public string ShipCustNo { get; set; }

        [JsonProperty("taxcode_1")]
        public string TaxCode1 { get; set; }

        [JsonProperty("tax_amount_1")]
        public decimal TaxAmount1 { get; set; }

        [JsonProperty("taxcode_2")]
        public string TaxCode2 { get; set; }

        [JsonProperty("tax_amount_2")]
        public decimal TaxAmount2 { get; set; }

        [JsonProperty("ship_address")]
        public AddressDto ShipAddress { get; set; }
    }
}
