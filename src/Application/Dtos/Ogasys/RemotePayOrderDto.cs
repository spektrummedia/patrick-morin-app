﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class RemotePayOrderDto
    {
        public string EPaymentId { get; set; }
        public string Type { get; set; }
        public string Language { get; set; }
        public AmountDto Amount { get; set; }
        public ContactDto Contact { get; set; }
        public OrderDto Order { get; set; }
    }
}
