﻿
namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class RefundItemDto
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string Reference { get; set; }
        public string ReceiptNumber { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
