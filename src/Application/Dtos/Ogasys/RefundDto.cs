﻿using System.Collections.Generic;

namespace PatrickMorin.Application.Dtos.Ogasys
{
    public class RefundDto
    {
        public decimal RefundAmountCompleted { get; set; }
        public decimal RefundAmountNotCompleted { get; set; }
        public bool RefundNotificationSent { get; set; }
        public List<RefundItemDto> RefundResults { get; set; } = new();
    }
}
