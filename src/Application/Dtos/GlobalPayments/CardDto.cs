﻿
namespace PatrickMorin.Application.Dtos.GlobalPayments
{
    public class CardDto
    {
        public string CardNumber { get; set; }
        public string CardholderName { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
        public string SecurityCode { get; set; }
    }
}
