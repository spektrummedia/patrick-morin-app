﻿
using System;

namespace PatrickMorin.Application.Dtos.GlobalPayments
{
    public class TransactionDto
    {
        public string OrderId { get; set; }
        public string AuthorizationCode { get; set; }
        public string PaymentsReference { get; set; }
        public string SchemeId { get; set; }
        public decimal AuthorizedAmount { get; set; }
        public string CardType { get; set; }
        public DateTime? TimeStamp { get; set; }
    }
}
