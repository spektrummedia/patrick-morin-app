﻿
namespace PatrickMorin.Application.Dtos.GlobalPayments
{
    public class TransactionDataDto
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string CustNo { get; set; }
        public string OrderNo { get; set; }
    }
}
