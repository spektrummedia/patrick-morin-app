﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Enums;
using Spk.Common.Helpers.String;

namespace PatrickMorin.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly CultureInfo _ci;
        private readonly ResourceManager _res;

        private readonly ILogger<OrderService> _logger;
        private readonly IOgasysService _ogasysService;
        private readonly IGlobalPaymentService _globalPaymentService;
        private readonly INotificationService _notificationService;

        public static readonly List<string> LockedOrderNos = new();

        public OrderService(ILogger<OrderService> logger, IOgasysService ogasysService, 
            IGlobalPaymentService globalPaymentService, INotificationService notificationService)
        {
            _logger = logger;
            _ogasysService = ogasysService;
            _globalPaymentService = globalPaymentService;
            _notificationService = notificationService;

            _ci = CultureInfo.GetCultureInfo("fr-CA");
            _res = new ResourceManager(Contants.ERRORS_RESOURCE_FILE_PATH, typeof(OrderService).Assembly);
        }

        public async Task<PaymentResultDto> Payment(CardDto card, RemotePayOrderDto order)
        {
            var result = new PaymentResultDto
            {
                Success = false,
                Message = string.Empty,
                Order = order
            };

            if (LockedOrderNos.Contains(order.Order.Header.OrderNo))
            {
                result.Message = _res.GetString("TransactionInProgressForOrder", _ci);
                return result;
            }

            LockedOrderNos.Add(order.Order.Header.OrderNo);
            var (transaction, message) = _globalPaymentService.ProcessAuthorization(card, order);
            if (transaction == null)
            {
                result.Message = message;
                LockedOrderNos.Remove(order.Order.Header.OrderNo);
                return result;
            }

            var (captureSuccess, captureMessage) = _globalPaymentService.CaptureTransaction(transaction);
            if (!captureSuccess)
            {
                result.Message = captureMessage;
                LockedOrderNos.Remove(order.Order.Header.OrderNo);
                return result;
            }

            var updatedOrder = await _ogasysService.ConfirmPayment(order.EPaymentId, transaction, card);
            if (updatedOrder?.Order == null)
            {
                LockedOrderNos.Remove(order.Order.Header.OrderNo);
                return result;
            }

            if (updatedOrder.Contact?.Number != null && updatedOrder.Contact?.Type != null &&
                updatedOrder.Order.Header?.OrderNo != null && transaction.TimeStamp.HasValue)
            {
                var success = await _notificationService.NotifyPaymentReceived(
                    updatedOrder.Contact.Type, updatedOrder.Contact.Number, order.Amount.Balance,
                    updatedOrder.Order.Header.OrderNo, transaction.TimeStamp.Value);

                if (!success)
                    _logger.LogError($"OrderService - Payment : Notification was not sent to {updatedOrder.Contact.Number}.");
            }

            result.Success = true;
            result.Order = updatedOrder;
            LockedOrderNos.Remove(order.Order.Header.OrderNo);

            return result;
        }

        public async Task<RefundDto> Refund(List<RefundItemDto> refunds, ContactDto contact, string orderNo)
        {
            var refundResult = new RefundDto();
            foreach (var refund in refunds)
            {
                var (transaction, message) = _globalPaymentService
                    .RefundCapturedTransaction(refund.Reference, refund.ReceiptNumber, refund.Amount);

                refund.ErrorCode = transaction != null ? 0 : 1;
                refund.ErrorMessage = message.IsNullOrWhiteSpace() ? string.Empty : message;

                if (transaction != null)
                    refundResult.RefundAmountCompleted += refund.Amount;
                else
                    refundResult.RefundAmountNotCompleted += refund.Amount;

                refundResult.RefundResults.Add(refund);
            }

            refundResult.RefundNotificationSent = false;
            if (refundResult.RefundAmountCompleted > 0)
            {
                var success = await _notificationService.NotifyRefundCompleted(contact.Type, contact.Number,
                    refundResult.RefundAmountCompleted, orderNo);

                refundResult.RefundNotificationSent = success;
                if (!success)
                    _logger.LogError($"OrderService - Refund : Notification was not sent to {contact.Number}.");
            }

            return refundResult;
        }
    }
}
