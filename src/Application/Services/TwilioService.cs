﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PatrickMorin.Application.Configuration;
using Spk.Common.Helpers.String;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace PatrickMorin.Application.Services
{
    public class TwilioService : ITwilioService
    {
        private readonly TwilioSettings _settings;
        private readonly ILogger<TwilioService> _logger;

        public TwilioService(IOptions<TwilioSettings> settings, ILogger<TwilioService> logger)
        {
            _logger = logger;
            _settings = settings.Value;
        }

        public bool SendTextMessage(string to, string message)
        {
            var success = false;
            if (!to.IsNullOrWhiteSpace() && Regex.Match(to, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}").Success)
            {
                var fromPhoneNumber = _settings.FromNumber;
                var toPhoneNumber = new PhoneNumber("+1" + to.Replace("-", "").Replace(" ", ""));

                try
                {
                    MessageResource.Create(toPhoneNumber,
                        from: new PhoneNumber(fromPhoneNumber), // Twilio assigned phone number.
                        body: message
                    );
                    success = true;
                }
                catch (Twilio.Exceptions.ApiException apiException)
                {
                    _logger.LogError($"The 'To' number + {toPhoneNumber} is not a valid phone number : {apiException.Message}");
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error occured {e.Message}.");
                }
            }
            else
            {
                _logger.LogWarning($"Text was not sent, destination phone number is not valid. ({to})");
            }
            return success;
        }
    }
}
