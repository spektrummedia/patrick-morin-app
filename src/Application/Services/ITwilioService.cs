﻿
namespace PatrickMorin.Application.Services
{
    public interface ITwilioService
    {
        bool SendTextMessage(string to, string message);
    }
}
