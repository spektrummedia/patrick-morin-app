﻿using System;
using System.Globalization;
using System.Resources;
using System.Threading.Tasks;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Extensions;
using PatrickMorin.Application.Mailing;
using PatrickMorin.Application.Mailing.Templates;

namespace PatrickMorin.Application.Services
{
    public class NotificationService : INotificationService
    {
        private readonly CultureInfo _ci;
        private readonly ResourceManager _res;

        private readonly IEmailFactory _emailFactory;
        private readonly ITwilioService _twilioService;

        public NotificationService(IEmailFactory emailFactory, ITwilioService twilioService)
        {
            _emailFactory = emailFactory;
            _twilioService = twilioService;
            _ci = CultureInfo.GetCultureInfo("fr-CA");
            _res = new ResourceManager(Contants.RESOURCE_FILE_PATH, typeof(NotificationService).Assembly);
        }

        public async Task<bool> NotifyOrderReceived(string contactMethod, string contactValue, string orderNo, string orderUrl)
        {
            return contactMethod switch
            {
                ContactTypes.EMAIL => await SendOrderReceivedEmail(contactValue, orderNo, orderUrl),
                ContactTypes.PHONE => SendOrderReceivedTextMessage(contactValue, orderNo, orderUrl),
                _ => false
            };
        }

        public async Task<bool> NotifyPaymentReceived(string contactMethod, string contactValue, decimal amount, string orderNo, DateTime date)
        {
            return contactMethod switch
            {
                ContactTypes.EMAIL => await SendPaymentReceivedEmail(contactValue, amount, orderNo, date),
                ContactTypes.PHONE => SendPaymentReceivedTextMessage(contactValue, amount, orderNo, date),
                _ => false
            };
        }

        public async Task<bool> NotifyRefundCompleted(string contactMethod, string contactValue, decimal amount, string orderNo)
        {
            return contactMethod switch
            {
                ContactTypes.EMAIL => await SendRefundCompletedEmail(contactValue, amount, orderNo),
                ContactTypes.PHONE => SendRefundCompletedTextMessage(contactValue, amount, orderNo),
                _ => false
            };
        }

        private async Task<bool> SendOrderReceivedEmail(string email, string orderNo, string orderUrl)
        {
            var signature = _res.GetString("Signature", _ci);
            var buttonTxt = _res.GetString("OrderReceivedButton", _ci);
            var greetings = _res.GetString("OrderReceivedGreeting", _ci);

            var subject = _res.GetString("OrderReceivedSubject", _ci);
            if (subject != null)
                subject = string.Format(subject, orderNo);

            var body = _res.GetString("OrderReceivedBody", _ci);
            if (body != null)
                body = string.Format(body, orderNo, string.Empty);

            var emailModel = new OrderReceivedEmail(email, subject, body, buttonTxt, orderUrl, greetings, signature);
            var preparedEmail = _emailFactory.Prepare(emailModel);
            var result = await preparedEmail.SendAsync();

            return result.Successful;
        }

        private bool SendOrderReceivedTextMessage(string phone, string orderNo, string orderUrl)
        {
            var message = _res.GetString("OrderReceivedBody", _ci);
            if (message != null)
                message = string.Format(message, orderNo, orderUrl);

            message += "\n" + _res.GetString("OrderReceivedGreeting", _ci) + "\n" + _res.GetString("Signature", _ci);

            return _twilioService.SendTextMessage(phone, message);
        }

        private async Task<bool> SendPaymentReceivedEmail(string email, decimal amount, string orderNo, DateTime date)
        {
            var greetings = _res.GetString("PaymentReceivedGreeting", _ci);

            var subject = _res.GetString("PaymentReceivedSubject", _ci);
            if (subject != null)
                subject = string.Format(subject, orderNo);

            var easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var localDateTime = TimeZoneInfo.ConvertTimeFromUtc(date, easternTimeZone);
            var body = _res.GetString("PaymentReceivedBody", _ci);
            if (body != null)
                body = string.Format(body, amount.ToPrice(), orderNo, localDateTime.ToString("M"), localDateTime.ToString("t"));

            var emailModel = new PaymentReceivedEmail(email, subject, body, greetings);
            var result = await _emailFactory.Prepare(emailModel).SendAsync();

            return result.Successful;
        }

        private bool SendPaymentReceivedTextMessage(string phone, decimal amount, string orderNo, DateTime date)
        {
            var easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var localDateTime = TimeZoneInfo.ConvertTimeFromUtc(date, easternTimeZone);
            var message = _res.GetString("PaymentReceivedBody", _ci);
            if (message != null)
                message = string.Format(message, amount.ToPrice(), orderNo, localDateTime.ToString("M"), localDateTime.ToString("t"));

            message += "\n\n" + _res.GetString("PaymentReceivedGreeting", _ci);

            return _twilioService.SendTextMessage(phone, message);
        }

        private async Task<bool> SendRefundCompletedEmail(string email, decimal amount, string orderNo)
        {
            var signature = _res.GetString("Signature", _ci);
            var greetings = _res.GetString("RefundSentGreeting", _ci);

            var subject = _res.GetString("RefundSentSubject", _ci);
            if (subject != null)
                subject = string.Format(subject, orderNo);

            var body = _res.GetString("RefundSentBody", _ci);
            if (body != null)
                body = string.Format(body, amount.ToPrice(), orderNo);

            var emailModel = new RefundCompletedEmail(email, subject, body, greetings, signature);
            var result = await _emailFactory.Prepare(emailModel).SendAsync();

            return result.Successful;
        }

        private bool SendRefundCompletedTextMessage(string phone, decimal amount, string orderNo)
        {
            var message = _res.GetString("RefundSentBody", _ci);
            if (message != null)
                message = string.Format(message, amount.ToPrice(), orderNo);

            message += "\n\n" + _res.GetString("RefundSentGreeting", _ci) + "\n" + _res.GetString("Signature", _ci);

            return _twilioService.SendTextMessage(phone, message);
        }
    }
}
