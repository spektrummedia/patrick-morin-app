﻿using GlobalPayments.Api.Entities;
using GlobalPayments.Api.PaymentMethods;
using PatrickMorin.Application.Dtos.GlobalPayments;

namespace PatrickMorin.Application.Services
{
    public class GlobalPaymentTransactionService : IGlobalPaymentTransactionService
    {
        public Transaction AuthorizeTransaction(CreditCardData creditCardData, TransactionDataDto data)
        {
            return creditCardData.Authorize(data.Amount)
                .WithDescription(data.Description)
                .WithCustomerId(data.CustNo)
                .WithClientTransactionId(data.OrderNo)
                .WithCurrency(data.Currency)
                .Execute();
        }

        public Transaction CaptureTransaction(Transaction transaction, decimal amount)
        {
            return transaction.Capture(amount)
                .Execute();
        }

        public Transaction RefundCapturedTransaction(Transaction transaction, decimal amount, string currency)
        {
            return transaction.Refund(amount)
                .WithCurrency(currency)
                .Execute();
        }
    }
}
