﻿using System.Threading.Tasks;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;

namespace PatrickMorin.Application.Services
{
    public interface IOgasysService
    {
        Task<RemotePayOrderDto> GetRemotePayOrder(string id);
        Task<RemotePayOrderDto> GetRemotePayOrderByOrderNo(string orderNo);
        Task<RemotePayOrderDto> ConfirmPayment(string ePaymentId, TransactionDto transaction, CardDto card);
        Task<RemotePayOrderDto> ConfirmRefund(string ePaymentId, TransactionDto transaction, string cardMask);
    }
}
