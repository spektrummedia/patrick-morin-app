﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PatrickMorin.Application.Configuration;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using Spk.Common.Helpers.String;

namespace PatrickMorin.Application.Services
{
    public class OgasysService : IOgasysService
    {
        private readonly OgasysSettings _settings;

        private readonly IHttpRequestService _httpRequestService;
        private readonly ILogger<OgasysService> _logger;

        private const string CARD_MASK = "XXXX XXXX XXXX ";

        public OgasysService(IOptions<OgasysSettings> settings, ILogger<OgasysService> logger, IHttpRequestService httpRequestService)
        {
            _logger = logger;
            _settings = settings.Value;
            _httpRequestService = httpRequestService;
        }

        public async Task<RemotePayOrderDto> GetRemotePayOrder(string ePaymentId)
        {
            if (ePaymentId.IsNullOrWhiteSpace())
                return null;

            var request = new HttpRequestMessage(HttpMethod.Get, _settings.Url + $"/ePayment/{ePaymentId}");
            var response = await _httpRequestService.SendAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                _logger.LogError($"OgasysService - GetRemotePayOrder : Error occured while getting order {ePaymentId} " +
                                 $": StatusCode = {response.StatusCode}.");
                return null;
            }

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RemotePayOrderDto>(content);
        }

        public async Task<RemotePayOrderDto> GetRemotePayOrderByOrderNo(string orderNo)
        {
            if (orderNo.IsNullOrWhiteSpace())
                return null;

            var request = new HttpRequestMessage(HttpMethod.Get, _settings.Url + $"/ePayment/order/{orderNo}");
            var response = await _httpRequestService.SendAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                _logger.LogError("OgasysService - GetRemotePayOrderByOrderNo : Error occured while getting order " +
                                 $"{orderNo} : StatusCode = {response.StatusCode}.");
                return null;
            }

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RemotePayOrderDto>(content);
        }

        public async Task<RemotePayOrderDto> ConfirmPayment(string ePaymentId, TransactionDto transaction, CardDto card)
        {
            if (ePaymentId.IsNullOrWhiteSpace() || transaction == null || card == null)
            {
                _logger.LogError("OgasysService - ConfirmPayment : Could not get order, either ePaymentId was null " +
                                 "empty or whitespace or transaction or card was null.");
                return null;
            }

            var cardNumber = card.CardNumber.Replace(" ", string.Empty);
            var cardMask = CARD_MASK + cardNumber.Substring(cardNumber.Length - 4);

            var payments = new PaymentListDto
            {
                Payment = new List<PaymentDto>
                {
                    new()
                    {
                        Amount = transaction.AuthorizedAmount,
                        Reference = transaction.PaymentsReference,
                        ReceiptNumber = transaction.OrderId,
                        Timestamp = DateTime.UtcNow,
                        CardNoMask = cardMask,
                        CardType = transaction.CardType,
                        Processor = Enums.Contants.PROCESSOR
                    }
                }
            };

            return await ConfirmTransaction(ePaymentId, payments);
        }

        public async Task<RemotePayOrderDto> ConfirmRefund(string ePaymentId, TransactionDto transaction, string cardMask)
        {
            if (ePaymentId.IsNullOrWhiteSpace() || transaction == null || cardMask.IsNullOrWhiteSpace())
            {
                _logger.LogError("OgasysService - ConfirmPayment : Could not get order, either ePaymentId or cardMask " +
                                 "was null empty or whitespace or transaction was null.");
                return null;
            }

            var payments = new PaymentListDto
            {
                Payment = new List<PaymentDto>
                {
                    new()
                    {
                        Amount = transaction.AuthorizedAmount * -1,
                        Reference = transaction.PaymentsReference,
                        ReceiptNumber = transaction.OrderId,
                        Timestamp = DateTime.UtcNow,
                        CardNoMask = cardMask,
                        CardType = transaction.CardType,
                        Processor = Enums.Contants.PROCESSOR
                    }
                }
            };

            return await ConfirmTransaction(ePaymentId, payments);
        }

        private async Task<RemotePayOrderDto> ConfirmTransaction(string ePaymentId, PaymentListDto payments)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, _settings.Url + $"/ePayment/{ePaymentId}");

            var json = JsonConvert.SerializeObject(payments);
            request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _httpRequestService.SendAsync(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                _logger.LogError("OgasysService - ConfirmTransaction : Error occured while confirming payment for order " +
                                 $"with ePaymentId {ePaymentId} : StatusCode = {response.StatusCode}.");
                return null;
            }

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RemotePayOrderDto>(content);
        }
    }
}
