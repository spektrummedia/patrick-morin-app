﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;

namespace PatrickMorin.Application.Services
{
    public interface IOrderService
    {
        Task<PaymentResultDto> Payment(CardDto card, RemotePayOrderDto order);
        Task<RefundDto> Refund(List<RefundItemDto> refunds, ContactDto contact, string orderNo);
    }
}
