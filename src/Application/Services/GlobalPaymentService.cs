﻿using System;
using System.Resources;
using GlobalPayments.Api;
using GlobalPayments.Api.Entities;
using GlobalPayments.Api.PaymentMethods;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PatrickMorin.Application.Configuration;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Enums;
using Spk.Common.Helpers.String;

namespace PatrickMorin.Application.Services
{
    public class GlobalPaymentService : IGlobalPaymentService
    {
        private readonly ResourceManager _res;
        private readonly GlobalPaymentSettings _settings;
        private readonly IGlobalPaymentTransactionService _transactionService;
        private readonly ILogger<GlobalPaymentService> _logger;
        
        public GlobalPaymentService(IOptions<GlobalPaymentSettings> settings, ILogger<GlobalPaymentService> logger,
            IGlobalPaymentTransactionService transactionService)
        {
            _logger = logger;
            _settings = settings.Value;
            _transactionService = transactionService;
            _res = new ResourceManager(Contants.ERRORS_RESOURCE_FILE_PATH, typeof(GlobalPaymentService).Assembly);

            ServicesContainer.ConfigureService(new GpEcomConfig
            {
                MerchantId = _settings.MerchantId,
                AccountId = _settings.Account,
                SharedSecret = _settings.SharedSecret,
                ServiceUrl = _settings.Url,
                RebatePassword = _settings.RebatePassword
            });
        }

        public Tuple<TransactionDto, string> ProcessAuthorization(CardDto cardDto, RemotePayOrderDto orderDto)
        {
            var amount = orderDto?.Amount?.Balance;
            if (amount == null || amount <= 0)
                return new Tuple<TransactionDto, string>(null, _res.GetString("OrderAlreadyPaid"));

            if (cardDto == null)
            {
                _logger.LogWarning("Could not process authorization because card is null.");
                return new Tuple<TransactionDto, string>(null, _res.GetString("ErrorOccuredDuringPayment"));
            }

            var card = new CreditCardData
            {
                Number = cardDto.CardNumber,
                ExpMonth = cardDto.ExpiryMonth,
                ExpYear = cardDto.ExpiryYear,
                Cvn = cardDto.SecurityCode,
                CardHolderName = cardDto.CardholderName
            };

            var message = "";
            TransactionDto transaction = null;
            try
            {
                var description = BuildTransactionDescription(orderDto);
                var data = new TransactionDataDto
                {
                    Amount = amount.Value,
                    Currency = _settings.Currency,
                    Description = description,
                    CustNo = orderDto.Order.Header.ShipCustNo,
                    OrderNo = orderDto.Order.Header.OrderNo
                };
                var response = _transactionService.AuthorizeTransaction(card, data);
                if (response.ResponseCode == "00") // 00 == Success 
                {
                    transaction = MapToTransactionDto(response, amount.Value, card.CardType);
                }
                else
                {
                    message = _res.GetString("ErrorOccuredDuringPayment");
                    _logger.LogError($"Delayed capture was not authorized -> (amount : {amount}$): " + 
                                     response.ResponseMessage);
                }
            }
            catch (ApiException ex)
            {
                message = HandleTransactionError(ex);
                _logger.LogError("Error occured during delayed capture authorization -> " +
                                 $"(amount : {amount}$, merchant_id : {_settings.MerchantId}) : {ex.Message}");
            }

            return new Tuple<TransactionDto, string>(transaction, message);
        }

        public Tuple<bool, string> CaptureTransaction(TransactionDto transactionDto)
        {
            if (transactionDto == null)
            {
                _logger.LogError("Could not capture transaction, transaction is null.");
                return new Tuple<bool, string>(false, _res.GetString("ErrorOccuredDuringPayment"));
            }

            var success = false;
            var message = "";
            var transaction = Transaction.FromId(transactionDto.PaymentsReference, transactionDto.OrderId);
            try
            {
                // send the capture request, we must specify the amount and currency
                var response = _transactionService.CaptureTransaction(transaction, transactionDto.AuthorizedAmount);

                if (response.ResponseCode == "00") // 00 == Success 
                {
                    success = true;
                }
                else
                {
                    message = _res.GetString("ErrorOccuredDuringPayment");
                    _logger.LogError($"Transaction was not captured -> {BuildTransactionLog(transactionDto)}" +
                                     $" : {response.ResponseMessage}");
                }
            }
            catch (ApiException ex)
            {
                message = HandleTransactionError(ex);
                _logger.LogError("Error occured during transaction capture -> " +
                                 $"{BuildTransactionLog(transactionDto)} : {ex.Message}");
            }
            return new Tuple<bool, string>(success, message);
        }

        public Tuple<TransactionDto, string> RefundCapturedTransaction(string paymentsReference, string orderId, decimal refundAmount)
        {
            if (paymentsReference.IsNullOrWhiteSpace() || orderId.IsNullOrWhiteSpace() || refundAmount <= 0)
            {
                _logger.LogError($"Could not do refund because paymentsReference or orderId is null " +
                                 "or amount is less or equal to zero.");
                return new Tuple<TransactionDto, string>(null, _res.GetString("ErrorOccuredDuringRefund"));
            }

            var authCode = "12345";
            var transaction = Transaction.FromId(paymentsReference, orderId);
            transaction.AuthorizationCode = authCode;

            var message = "";
            TransactionDto resultTransaction = null;
            try
            {
                var response = _transactionService.RefundCapturedTransaction(transaction, refundAmount, _settings.Currency);
                if (response.ResponseCode == "00") // 00 == Success 
                {
                    resultTransaction = MapToTransactionDto(response, refundAmount);
                }
                else
                {
                    message = _res.GetString("ErrorOccuredDuringRefund");
                    _logger.LogError("Refund was not captured -> " +
                                     $"{BuildRefundLog(paymentsReference, orderId, authCode, refundAmount)} : " +
                                     $"{response.ResponseMessage}");
                }
            }
            catch (ApiException ex)
            {
                message = HandleTransactionError(ex, true);
                _logger.LogError("Error occured during refund of captured transaction -> " +
                                 $"{BuildRefundLog(paymentsReference, orderId, authCode, refundAmount)} : {ex.Message}");
            }

            return new Tuple<TransactionDto, string>(resultTransaction, message);
        }

        private string HandleTransactionError(ApiException ex, bool isRefund = false)
        {
            if (ex.Message.Contains(GlobalPaymentErrors.BANK_DECLINED))
                return _res.GetString("DeclinedByBank");

            if (ex.Message.Contains(GlobalPaymentErrors.REF_B_AUTHORIZATION_ERROR))
                return _res.GetString("AuthorizationError");

            if (ex.Message.Contains(GlobalPaymentErrors.REF_A_REPORTED_STOLEN))
                return _res.GetString("ReportedStolen");

            if (ex.Message.Contains(GlobalPaymentErrors.CREDIT_CARD) || ex.Message.Contains(GlobalPaymentErrors.REFUND_ERROR))
            {
                var hyphenIndex = ex.Message.IndexOf("- ", StringComparison.InvariantCulture);
                return ex.Message.Substring(hyphenIndex + 2);
            }

            return isRefund ? _res.GetString("ErrorOccuredDuringRefund") : _res.GetString("ErrorOccuredDuringPayment");
        }

        private TransactionDto MapToTransactionDto(Transaction transaction, decimal amount, string cardType = null)
        {
            DateTime? timeStamp = null;
            if (!transaction.Timestamp.IsNullOrWhiteSpace() && transaction.Timestamp.Length > 13)
            {
                //"20210315164455"
                var year = transaction.Timestamp.Substring(0, 4).ToInt32();
                var month = transaction.Timestamp.Substring(4, 2).ToInt32();
                var day = transaction.Timestamp.Substring(6, 2).ToInt32();
                var hour = transaction.Timestamp.Substring(8, 2).ToInt32();
                var minute = transaction.Timestamp.Substring(10, 2).ToInt32();
                var seconds = transaction.Timestamp.Substring(12, 2).ToInt32();
                timeStamp = new DateTime(year, month, day, hour, minute, seconds);
            }
            return new()
            {
                OrderId = transaction.OrderId,
                AuthorizationCode = transaction.AuthorizationCode,
                PaymentsReference = transaction.TransactionId,
                SchemeId = transaction.SchemeId,
                AuthorizedAmount = amount,
                CardType = cardType,
                TimeStamp = timeStamp
            };
        }

        private string BuildTransactionDescription(RemotePayOrderDto orderDto)
        {
            var description = "";
            if (orderDto.Contact != null)
            {
                description =
                    $"Nom: {orderDto.Contact.Name} | {(orderDto.Contact.Type == ContactTypes.PHONE ? "Tel" : "Email")}" +
                    $": {orderDto.Contact.Number}";
            }

            var address = orderDto.Order?.Header?.ShipAddress;
            if (address != null)
            {
                description += $" | Rue: {address.Address1} | Ville: {address.City} | Province" +
                               $": {address.Province} | ZIP: {address.Postcode} | Pays: {address.Country}";
            }
            return description;
        }

        private string BuildTransactionLog(TransactionDto transaction)
        {
            return $"OrderId = {transaction.OrderId}, " +
                   $"AuthorizationCode = {transaction.AuthorizationCode}, " +
                   $"TransactionId/PaymentsReference = {transaction.PaymentsReference}, " +
                   $"SchemeId = {transaction.SchemeId}, " +
                   $"AuthorizedAmount = {transaction.AuthorizedAmount}";
        }

        private string BuildRefundLog(string paymentsReference, string orderId, string authCode, decimal amount)
        {
            return $"TransactionId/PaymentsReference = {paymentsReference}, " +
                   $"OrderId = {orderId}, " +
                   $"AuthorizationCode = {authCode}, " +
                   $"Amount = {amount}";
        }
    }
}
