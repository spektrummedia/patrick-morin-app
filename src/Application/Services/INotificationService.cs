﻿
using System;
using System.Threading.Tasks;

namespace PatrickMorin.Application.Services
{
    public interface INotificationService
    {
        Task<bool> NotifyOrderReceived(string contactMethod, string contactValue, string orderNo, string orderUrl);
        Task<bool> NotifyRefundCompleted(string contactMethod, string contactValue, decimal amount, string orderNo);
        Task<bool> NotifyPaymentReceived(string contactMethod, string contactValue, decimal amount, string orderNo, DateTime date);
    }
}
