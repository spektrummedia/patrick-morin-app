﻿using GlobalPayments.Api.Entities;
using GlobalPayments.Api.PaymentMethods;
using PatrickMorin.Application.Dtos.GlobalPayments;

namespace PatrickMorin.Application.Services
{
    public interface IGlobalPaymentTransactionService
    {
        Transaction AuthorizeTransaction(CreditCardData creditCardData, TransactionDataDto data);
        Transaction CaptureTransaction(Transaction transaction, decimal amount);
        Transaction RefundCapturedTransaction(Transaction transaction, decimal amount, string currency);
    }
}
