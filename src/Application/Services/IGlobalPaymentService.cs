﻿using System;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;

namespace PatrickMorin.Application.Services
{
    public interface IGlobalPaymentService
    {
        Tuple<TransactionDto, string> ProcessAuthorization(CardDto card, RemotePayOrderDto orderDto);
        Tuple<bool, string> CaptureTransaction(TransactionDto transactionDto);
        Tuple<TransactionDto, string> RefundCapturedTransaction(string paymentsReference, string orderId, decimal refundAmount);
    }
}
