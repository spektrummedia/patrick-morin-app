﻿using System.Net.Http;
using System.Threading.Tasks;

namespace PatrickMorin.Application.Services
{
    public interface IHttpRequestService
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
    }
}
