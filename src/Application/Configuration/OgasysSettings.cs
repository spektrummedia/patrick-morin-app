﻿
namespace PatrickMorin.Application.Configuration
{
    public class OgasysSettings
    {
        public string Url { get; set; }
        public string AccessKey { get; set; }
    }
}
