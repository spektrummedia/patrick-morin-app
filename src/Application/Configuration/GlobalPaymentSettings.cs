﻿
namespace PatrickMorin.Application.Configuration
{
    public class GlobalPaymentSettings
    {
        public string Url { get; set; }
        public string MerchantId { get; set; }
        public string Account { get; set; }
        public string SharedSecret { get; set; }
        public string RebatePassword { get; set; }
        public string Currency { get; set; }
    }
}
