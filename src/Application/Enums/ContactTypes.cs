﻿
namespace PatrickMorin.Application.Enums
{
    public static class ContactTypes
    {
        public const string EMAIL = "EMAIL";
        public const string PHONE = "PHONE";
    }
}
