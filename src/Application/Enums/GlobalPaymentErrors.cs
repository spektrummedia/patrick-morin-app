﻿
namespace PatrickMorin.Application.Enums
{
    public static class GlobalPaymentErrors
    {
        public const string BANK_DECLINED = "101";
        public const string REF_B_AUTHORIZATION_ERROR = "102";
        public const string REF_A_REPORTED_STOLEN = "103";
        public const string CREDIT_CARD = "509";
        public const string REFUND_ERROR = "512";
    }
}
