﻿
namespace PatrickMorin.Application.Enums
{
    public static class Contants
    {
        public const string RESOURCE_FILE_PATH = "PatrickMorin.Application.Resources.NotificationMessages";
        public const string ERRORS_RESOURCE_FILE_PATH = "PatrickMorin.Application.Resources.ErrorMessages";
        public const string PROCESSOR = "GlobalPaym";
    }
}
