﻿
namespace PatrickMorin.Application.Mailing.Templates
{
    public class RefundCompletedEmail : EmailModelBase
    {
        public string Body { get; set; }
        public string Greetings { get; set; }
        public string Signature { get; set; }

        public RefundCompletedEmail(string to, string subject, string body, string greetings, string signature) : base(subject, to)
        {
            Body = body;
            Greetings = greetings;
            Signature = signature;
        }
    }
}
