﻿
namespace PatrickMorin.Application.Mailing.Templates
{
    public class PaymentReceivedEmail : EmailModelBase
    {
        public string Body { get; set; }
        public string Greetings { get; set; }

        public PaymentReceivedEmail(string to, string subject, string body, string greetings) : base(subject, to)
        {
            Body = body;
            Greetings = greetings;
        }
    }
}
