﻿
namespace PatrickMorin.Application.Mailing.Templates
{
    public class OrderReceivedEmail : EmailModelBase
    {
        public string Body { get; set; }
        public string ButtonText { get; set; }
        public string Url { get; set; }
        public string Greetings { get; set; }
        public string Signature { get; set; }

        public OrderReceivedEmail(string to, string subject, string body, string buttonText, 
            string url, string greetings, string signature) : base(subject, to)
        {
            Body = body;
            ButtonText = buttonText;
            Url = url;
            Greetings = greetings;
            Signature = signature;
        }
    }
}
