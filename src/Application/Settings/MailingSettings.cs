﻿namespace PatrickMorin.Application.Settings
{
    public class MailingSettings
    {
        public string FromName { get; set; }
        public string FromAddress { get; set; }
    }
}