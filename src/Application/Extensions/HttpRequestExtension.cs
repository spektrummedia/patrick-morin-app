﻿using Microsoft.AspNetCore.Http;

namespace PatrickMorin.Application.Extensions
{
    public static class HttpRequestExtension
    {
        public static string BaseUrl(this HttpRequest req)
        {
            return req.Scheme + "://" + req.Host;
        }
    }
}
