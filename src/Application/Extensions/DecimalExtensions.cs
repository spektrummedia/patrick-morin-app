namespace PatrickMorin.Application.Extensions
{
    public static class DecimalExtensions
    {
        public static string ToPrice(this decimal price)
        {
            return price == 0 ? $"{price}0" : $"{price:.00}";
        }
    }
}