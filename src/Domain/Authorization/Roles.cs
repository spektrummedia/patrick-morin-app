﻿using System;
using System.Collections.Generic;
using PatrickMorin.Domain.Entities;

namespace PatrickMorin.Domain.Authorization
{
    public static class Roles
    {
        public static IEnumerable<Role> GetAllRoles()
        {
            var roles = typeof(Constants.Authorization.Roles).GetFields();

            foreach (var field in roles)
            {
                var roleName = field.GetValue(null) as string;
                yield return new Role
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = roleName
                };
            }
        }
    }
}