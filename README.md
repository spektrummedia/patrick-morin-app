# SPK Web Application Template

## Azure prerequisite
1. In the subscription create a resource group
2. In the resource group previously created create a storage account
3. In the storage account create a blob container.  
3.1 Set blob container access policy to anonymous read access