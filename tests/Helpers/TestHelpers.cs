﻿using Microsoft.AspNetCore.Mvc;

namespace PatrickMorin.Tests.Helpers
{
    public static class TestHelpers
    {
        public static string GetBadRequestMessage(IActionResult result)
        {
            return (string)((BadRequestObjectResult)result).Value;
        }

        public static string GetNotFoundMessage(IActionResult result)
        {
            return (string)((NotFoundObjectResult)result).Value;
        }

        public static T GetResultObject<T>(IActionResult result)
        {
            return (T)((OkObjectResult)result).Value;
        }
    }
}
