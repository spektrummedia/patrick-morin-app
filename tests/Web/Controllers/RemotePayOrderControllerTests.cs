using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Services;
using PatrickMorin.Tests.Helpers;
using PatrickMorin.Web.Controllers;
using PatrickMorin.Web.Models;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Web.Controllers
{
    public class RemotePayOrderControllerTests
    {
        private readonly Mock<ILogger<RemotePayOrderController>> _logger;

        private readonly Mock<IOrderService> _orderService;
        private readonly Mock<IOgasysService> _ogasysService;
        private readonly Mock<INotificationService> _notificationService;

        public RemotePayOrderControllerTests()
        {
            _logger = new Mock<ILogger<RemotePayOrderController>>();
            _orderService = new Mock<IOrderService>();
            _ogasysService = new Mock<IOgasysService>();
            _notificationService = new Mock<INotificationService>();
        }

        [Fact]
        public async Task Notify_ShouldReturnBadRequest_WhenContactTypeIsNotSupported()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = "rmgrondin@spektrummedia.com", Type = "dumb type" }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe($"Contact type {remotePayOrderDto.Contact.Type} is not valid.");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task Notify_ShouldReturnBadRequest_WhenEPaymentIdIsNullEmptyOrWhiteSpace(string ePaymentId)
        {
            // Arrange
            var controller = CreateController();

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe("You must set a ePaymentId.");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task Notify_ShouldReturnBadRequest_WhenNoEmailIsSet(string email)
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = email, Type = ContactTypes.EMAIL}
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe($"Could not reach customer for order {ePaymentId} no phone or email is set.");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task Notify_ShouldReturnBadRequest_WhenNoPhoneIsSet(string phone)
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = phone, Type = ContactTypes.PHONE }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe($"Could not reach customer for order {ePaymentId} no phone or email is set.");
        }

        [Fact]
        public async Task Notify_ShouldReturnInternalServerError_WhenEmailFactoryResponseReturnsSuccessfulFalse()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = "rmgrondin@spektrummedia.com", Type = ContactTypes.EMAIL }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            var email = new Mock<Email>();
            email
                .Setup(x => x.SendAsync(null))
                .ReturnsAsync(new SendResponse { ErrorMessages = new List<string> { "Error" } });

            _notificationService
                .Setup(x => x.NotifyOrderReceived(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync(false);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var objectResult = (ObjectResult)result;
            var message = (string)objectResult.Value;

            // Assert
            result.ShouldBeOfType<ObjectResult>();
            objectResult.ShouldNotBeNull();
            objectResult.StatusCode.ShouldBe(500);
            message.ShouldBe($"Error occured while sending notification to {remotePayOrderDto.Contact.Number}.");
        }

        [Fact]
        public async Task Notify_ShouldReturnInternalServerError_WhenRemotePayOrderWithEPaymentIdIsNotFound()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync((RemotePayOrderDto) null);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var message = TestHelpers.GetNotFoundMessage(result);

            // Assert
            result.ShouldBeOfType<NotFoundObjectResult>();
            message.ShouldBe($"Order or contact is null for order {ePaymentId}.");
        }

        [Fact]
        public async Task Notify_ShouldReturnInternalServerError_WhenTwilioServiceReturnsSuccessfulFalse()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = "418 222-1111", Type = ContactTypes.PHONE }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });
            var objectResult = (ObjectResult)result;
            var message = (string)objectResult.Value;

            // Assert
            result.ShouldBeOfType<ObjectResult>();
            objectResult.ShouldNotBeNull();
            objectResult.StatusCode.ShouldBe(500);
            message.ShouldBe($"Error occured while sending notification to {remotePayOrderDto.Contact.Number}.");
        }

        [Fact]
        public async Task Notify_ShouldSendEmailAndReturnOkResult_WhenEmailIsSet()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = "rmgrondin@spektrummedia.com", Type = ContactTypes.EMAIL }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            _notificationService
                .Setup(x => x.NotifyOrderReceived(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), 
                    It.IsAny<string>()))
                .ReturnsAsync(true);

            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });

            // Assert
            result.ShouldBeOfType<OkResult>();
        }

        [Fact]
        public async Task Notify_ShouldSendTextMessageAndReturnOkResult_WhenPhoneIsSet()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var controller = CreateController();

            var remotePayOrderDto = new RemotePayOrderDto
            {
                Contact = new ContactDto { Number = "4181112222", Type = ContactTypes.PHONE }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(ePaymentId))
                .ReturnsAsync(remotePayOrderDto);

            _notificationService
                .Setup(x => x.NotifyOrderReceived(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync(true);
            // Act
            var result = await controller.Notify(new RemotePayOrderModel { EPaymentId = ePaymentId });

            // Assert
            result.ShouldBeOfType<OkResult>();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task Refund_ShouldReturnBadRequest_WhenContactNumberIsNullEmptyOrWhiteSpace(string number)
        {
            // Arrange
            var model = new RefundModel
            {
                Contact = new ContactDto { Type = ContactTypes.PHONE, Number = number },
                Refunds = new List<RefundItemModel>(),
                Order = new OrderDto { Header = new HeaderDto { OrderNo = "" } }
            };
            var controller = CreateController(model);

            // Act
            var result = await controller.Refund();
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe($"Could not reach customer for order {model.EPaymentId} no phone or email is set.");
        }

        [Fact]
        public async Task Refund_ShouldReturnBadRequest_WhenContactTypeIsNotSupported()
        {
            // Arrange
            var model = new RefundModel
            {
                Contact = new ContactDto { Type = "Unknown" },
                Refunds = new List<RefundItemModel>(),
                Order = new OrderDto { Header = new HeaderDto { OrderNo = "" } }
            };
            var controller = CreateController(model);

            // Act
            var result = await controller.Refund();
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe($"Contact type {model.Contact.Type} is not valid.");
        }

        [Fact]
        public async Task Refund_ShouldReturnBadRequest_WhenModelIsNull()
        {
            // Arrange
            var controller = CreateController();

            // Act
            var result = await controller.Refund();
            var message = TestHelpers.GetBadRequestMessage(result);

            // Assert
            result.ShouldBeOfType<BadRequestObjectResult>();
            message.ShouldBe("Model, refund list or order_no cannot be null.");
        }

        [Fact]
        public async Task Refund_ShouldReturnNotFound_WhenModelContactIsNull()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var model = new RefundModel
            {
                EPaymentId = ePaymentId,
                Refunds = new List<RefundItemModel>(),
                Order = new OrderDto {Header = new HeaderDto {OrderNo = ""}}
            };
            var controller = CreateController(model);

            // Act
            var result = await controller.Refund();
            var message = TestHelpers.GetNotFoundMessage(result);

            // Assert
            result.ShouldBeOfType<NotFoundObjectResult>();
            message.ShouldBe($"Contact is null for order {ePaymentId}.");
        }

        [Fact]
        public async Task Refund_ShouldReturnRefundDto_WhenRefundIsDone()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var model = new RefundModel
            {
                EPaymentId = ePaymentId,
                Contact = new ContactDto { Type = ContactTypes.PHONE, Number = "418 222-9999" },
                Refunds = new List<RefundItemModel> { new()
                {
                    Id = "Id",
                    Amount = 30,
                    Reference = "ref1",
                    ReceiptNumber = "orderno1",
                }},
                Order = new OrderDto { Header = new HeaderDto { OrderNo = "" } }
            };

            _orderService
                .Setup(x => x.Refund(It.IsAny<List<RefundItemDto>>(), It.IsAny<ContactDto>(), It.IsAny<string>()))
                .ReturnsAsync(new RefundDto());

            var controller = CreateController(model);

            // Act
            var result = await controller.Refund();
            var refundDto = TestHelpers.GetResultObject<RefundDto>(result);

            // Assert
            result.ShouldBeOfType<OkObjectResult>();
            refundDto.ShouldNotBeNull();
        }

        private RemotePayOrderController CreateController(object objInStream = null)
        {
            var controller = new RemotePayOrderController(_logger.Object, _ogasysService.Object, 
                _notificationService.Object, _orderService.Object);

            var request = new Mock<HttpRequest>();
            request.SetupGet(x => x.Scheme).Returns("https");
            request.SetupGet(x => x.Host).Returns(new HostString("patrickmorin.local", 5000));

            var bodyString = JsonConvert.SerializeObject(objInStream);
            request.Setup(x => x.Body).Returns(new MemoryStream(Encoding.UTF8.GetBytes(bodyString)));
            
            var context = new Mock<HttpContext>();
            context.SetupGet(x => x.Request).Returns(request.Object);

            var controllerContext = new ControllerContext { HttpContext = context.Object };
            controller.ControllerContext = controllerContext;

            return controller;
        }
    }
}
