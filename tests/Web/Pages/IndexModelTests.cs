using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Moq;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Services;
using PatrickMorin.Web.Pages;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Web.Pages
{
    public class IndexModelTests
    {
        private readonly Mock<ILogger<IndexModel>> _logger;
        private readonly Mock<IOgasysService> _ogasysService;

        public IndexModelTests()
        {
            _logger = new Mock<ILogger<IndexModel>>();
            _ogasysService = new Mock<IOgasysService>();
        }

        [Fact]
        public async Task OnPostAsync_ShouldReturnPageResult_WhenModelStateIsInvalid()
        {
            // Arrange
            var indexModel = CreateModel(null);
            indexModel.ModelState.AddModelError("OrderNo", "IsRequired");

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<PageResult>();
        }

        [Fact]
        public async Task OnPostAsync_ShouldReturnRedirectToPageResult_WhenOrderNoIsValid()
        {
            // Arrange
            var indexModel = CreateModel("07C678299");
            _ogasysService
                .Setup(x => x.GetRemotePayOrderByOrderNo(It.IsAny<string>()))
                .ReturnsAsync(new RemotePayOrderDto {EPaymentId = "4420674730640090902"});

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnPostAsync_ShouldReturnRedirectToPageResult_WhenOrderNoIsInValid()
        {
            // Arrange
            var indexModel = CreateModel("07C678299");

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        private IndexModel CreateModel(string orderNo)
        {
            return new(_ogasysService.Object) {OrderNo = orderNo};
        }
    }
}
