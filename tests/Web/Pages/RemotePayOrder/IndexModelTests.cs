using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Services;
using PatrickMorin.Web.Pages.RemotePayOrder;
using Shouldly;
using Xunit;
using Moq;
using PatrickMorin.Web.Models;

namespace PatrickMorin.Tests.Web.Pages.RemotePayOrder
{
    public class IndexModelTests
    {
        private readonly Mock<ILogger<IndexModel>> _logger;
        private readonly Mock<IOrderService> _orderService;
        private readonly Mock<IOgasysService> _ogasysService;
        private readonly Mock<IGlobalPaymentService> _globalPaymentService;
        private readonly Mock<INotificationService> _notificationService;

        public IndexModelTests()
        {
            _logger = new Mock<ILogger<IndexModel>>();
            _orderService = new Mock<IOrderService>();
            _ogasysService = new Mock<IOgasysService>();
            _globalPaymentService = new Mock<IGlobalPaymentService>();
            _notificationService = new Mock<INotificationService>();
        }

        [Fact]
        public async Task OnPostAsync_ShouldSetErrorMessageInTempData_WhenOrderServiceReturnsFalseWithMessage()
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902";
            indexModel.Card = new CardModel
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(indexModel.EPaymentId))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Order = new OrderDto(),
                    Amount = new AmountDto { Balance = 200 }
                });

            _orderService
                .Setup(x => x.Payment(It.IsAny<CardDto>(), It.IsAny<RemotePayOrderDto>()))
                .ReturnsAsync(new PaymentResultDto { Message = "Error message", Success = false});

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldNotBeNull();
            indexModel.TempData["PaymentSuccess"].ShouldNotBeNull();
            indexModel.TempData["PaymentSuccess"].ShouldBe(false);
            indexModel.TempData["PaymentMessage"].ShouldNotBeNull();
            indexModel.TempData["PaymentMessage"].ShouldBe("Error message");
        }

        [Fact]
        public async Task OnPostAsync_ShouldSetErrorMessageInTempData_WhenOrderServiceReturnsFalseWithEmptyMessage()
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902";
            indexModel.Card = new CardModel
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(indexModel.EPaymentId))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Order = new OrderDto(),
                    Amount = new AmountDto { Balance = 200 }
                });

            _orderService
                .Setup(x => x.Payment(It.IsAny<CardDto>(), It.IsAny<RemotePayOrderDto>()))
                .ReturnsAsync(new PaymentResultDto { Message = "Une erreur est survenue pendant le paiement.", Success = false });

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldNotBeNull();
            indexModel.TempData["PaymentSuccess"].ShouldNotBeNull();
            indexModel.TempData["PaymentSuccess"].ShouldBe(false);
            indexModel.TempData["PaymentMessage"].ShouldNotBeNull();
            indexModel.TempData["PaymentMessage"].ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Fact]
        public async Task OnPostAsync_ShouldReturnUpdatedOrder_WhenOrderServiceReturnsWithEmptyMessage()
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902";
            indexModel.Card = new CardModel
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            indexModel.Order = new RemotePayOrderDto
            {
                Order = new OrderDto(),
                Amount = new AmountDto { Balance = 200 }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(indexModel.EPaymentId))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Order = new OrderDto(),
                    Amount = new AmountDto { Balance = 200 }
                });

            _orderService
                .Setup(x => x.Payment(It.IsAny<CardDto>(), It.IsAny<RemotePayOrderDto>()))
                .ReturnsAsync(new PaymentResultDto { Message = string.Empty, Success = true });

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldNotBeNull();
            indexModel.TempData["PaymentSuccess"].ShouldNotBeNull();
            indexModel.TempData["PaymentSuccess"].ShouldBe(true);
            indexModel.TempData["PaymentMessage"].ShouldBeNull();
        }


        [Fact]
        public async Task OnGetAsync_ShouldReturnPageResultAndSetProperties()
        {
            // Arrange
            var orderId = "4420674730640090902";
            var indexModel = CreateModel();

            var order = new RemotePayOrderDto
            {
                Amount = new AmountDto(),
                Order = new OrderDto { Items = new OrderItemsDto { Item = new List<OrderItemDto> { new() } } }
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(orderId))
                .ReturnsAsync(order);

            // Act
            var result = await indexModel.OnGetAsync("4420674730640090902");

            // Assert
            result.ShouldBeOfType<PageResult>();
            indexModel.EPaymentId.ShouldBe(orderId);
            indexModel.Order.ShouldBe(order);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task OnGetAsync_ShouldReturnRedirectToPageResult_WhenEPaymentIdIsNullEmptyOrWhitespace(string id)
        {
            // Arrange
            var indexModel = CreateModel();

            // Act
            var result = await indexModel.OnGetAsync(id);

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnGetAsync_ShouldReturnRedirectToPageResult_WhenOgasysOrderAmountIsNull()
        {
            // Arrange
            var orderId = "4420674730640090902";
            var indexModel = CreateModel();

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(orderId))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Amount = null,
                    Order = new OrderDto
                    {
                        Items = new OrderItemsDto { Item = new List<OrderItemDto> { new OrderItemDto() } }
                    }
                });

            // Act
            var result = await indexModel.OnGetAsync("4420674730640090902");

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnGetAsync_ShouldReturnRedirectToPageResult_WhenOgasysOrderIsNotFound()
        {
            // Arrange
            var indexModel = CreateModel();

            // Act
            var result = await indexModel.OnGetAsync("4420674730640090902");

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnGetAsync_ShouldReturnRedirectToPageResult_WhenOgasysOrderItemsListIsEmpty()
        {
            // Arrange
            var orderId = "4420674730640090902";
            var indexModel = CreateModel();

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(orderId))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Amount = new AmountDto(),
                    Order = new OrderDto { Items = new OrderItemsDto { Item = new List<OrderItemDto>() } }
                });

            // Act
            var result = await indexModel.OnGetAsync("4420674730640090902");

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnGetAsync_ShouldReturnRedirectToPageResult_WhenOgasysOrderItemsListIsNull()
        {
            // Arrange
            var orderId = "4420674730640090902";
            var indexModel = CreateModel();

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(orderId))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Amount = new AmountDto(),
                    Order = new OrderDto { Items = null }
                });

            // Act
            var result = await indexModel.OnGetAsync("4420674730640090902");

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }


        [Fact]
        public async Task OnPostAsync_ShouldReturnPageResult_WhenExpirationDateIsInThePast()
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902";
            indexModel.Card = new CardModel
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = 2020,
                ExpiryMonth = 1,
                SecurityCode = "999"
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(indexModel.EPaymentId))
                .ReturnsAsync(new RemotePayOrderDto { Order = new OrderDto(), Amount = new AmountDto() });

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<PageResult>();
        }

        [Theory]
        [InlineData(0, 2021)]
        [InlineData(12, 0)]
        [InlineData(0, 0)]
        public async Task OnPostAsync_ShouldReturnPageResult_WhenExpirationMonthOrYearIsInvalid(int month, int year)
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902";
            indexModel.Card = new CardModel
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = year,
                ExpiryMonth = month,
                SecurityCode = "999"
            };

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(indexModel.EPaymentId))
                .ReturnsAsync(new RemotePayOrderDto { Order = new OrderDto(), Amount = new AmountDto() });

            if (month == 0)
                indexModel.ModelState.AddModelError("Card.ExpiryMonth", "");

            if (year == 0)
                indexModel.ModelState.AddModelError("Card.ExpiryYear", "");

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<PageResult>();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task OnPostAsync_ShouldReturnRedirectToPageResult_WhenEPaymentIdIsNullEmptyOrWhitespace(string ePaymentId)
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = ePaymentId;

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnPostAsync_ShouldReturnRedirectToPageResult_WhenRemotePayOrderDoesNotExist()
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902"; 

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        [Fact]
        public async Task OnPostAsync_ShouldReturnRedirectToPageResult_WhenRemotePayOrderOrderPropertyIsNull()
        {
            // Arrange
            var indexModel = CreateModel();
            indexModel.EPaymentId = "4420674730640090902";

            _ogasysService
                .Setup(x => x.GetRemotePayOrder(indexModel.EPaymentId))
                .ReturnsAsync(new RemotePayOrderDto {Order = null});

            // Act
            var result = await indexModel.OnPostAsync();

            // Assert
            result.ShouldBeOfType<RedirectToPageResult>();
        }

        private IndexModel CreateModel()
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            return new IndexModel(_logger.Object, _ogasysService.Object, _orderService.Object)
            {
                TempData = tempData
            };
        }
    }
}
