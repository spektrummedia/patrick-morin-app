﻿using FluentEmail.Core.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using Shouldly;
using PatrickMorin.Application.Mailing;
using PatrickMorin.Application.Settings;
using Xunit;

namespace PatrickMorin.Tests.Application.Mailing
{
    public class EmailFactoryTests
    {
        private const string FromAddress = "from@test.com";
        private const string FromName = "from me !";

        [Fact]
        public void Prepare_ShouldReturnemailInCorrectState_When()
        {
            //arrange
            var emailFact = ArrangeEmailFactory();

            //act
            var email = emailFact.Prepare("test@test.com", "Subject", "Message patate !");

            //assert
            email.Data.FromAddress.EmailAddress.ShouldBe(FromAddress);
            email.Data.FromAddress.Name.ShouldBe(FromName);
        }

        [Theory]
        [InlineData(",")]
        [InlineData(";")]
        public void Prepare_ShouldReturnCollectionOfRecipient_WhenProvidingMoreThan1To(string separator)
        {
            //arrange
            var emailFact = ArrangeEmailFactory();

            //act
            var email = emailFact.Prepare($"to1@test.com{separator}to2@test.com{separator}to3@test.com",
                "Subject", "Message patate !");

            //assert
            email.Data.FromAddress.EmailAddress.ShouldBe(FromAddress);

            Assert.Collection(email.Data.ToAddresses, a => a.EmailAddress.ShouldBe("to1@test.com"),
                a => a.EmailAddress.ShouldBe("to2@test.com"),
                a => a.EmailAddress.ShouldBe("to3@test.com"));
        }

        private static EmailFactory ArrangeEmailFactory()
        {
            var renderer = new Mock<ITemplateRenderer>();
            var config = new Mock<IConfiguration>();
            var mailingSettings = new Mock<IOptions<MailingSettings>>();
            mailingSettings.Setup(x => x.Value).Returns(new MailingSettings { FromAddress = FromAddress, FromName = FromName });

            var emailFact = new EmailFactory(renderer.Object, mailingSettings.Object, config.Object);
            return emailFact;
        }
    }
}