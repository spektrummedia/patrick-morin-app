using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Services;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Application.Services
{
    public class OrderServiceTests
    {
        private readonly Mock<ILogger<OrderService>> _logger;
        private readonly Mock<IOgasysService> _ogasysService;
        private readonly Mock<IGlobalPaymentService> _globalPaymentService;
        private readonly Mock<INotificationService> _notificationService;

        public OrderServiceTests()
        {
            _logger = new Mock<ILogger<OrderService>>();
            _ogasysService = new Mock<IOgasysService>();
            _globalPaymentService = new Mock<IGlobalPaymentService>();
            _notificationService = new Mock<INotificationService>();
        }

        [Fact]
        public async Task Payment_ShouldLogErrorButReturnSuccessTrue_WhenNotificationWasNotSentSuccessfully()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var order = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Contact = new ContactDto(),
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            _globalPaymentService
                .Setup(x => x.ProcessAuthorization(card, order))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto { TimeStamp = DateTime.UtcNow }, string.Empty));

            _globalPaymentService
                .Setup(x => x.CaptureTransaction(It.IsAny<TransactionDto>()))
                .Returns(new Tuple<bool, string>(true, string.Empty));

            _ogasysService
                .Setup(x => x.ConfirmPayment(order.EPaymentId, It.IsAny<TransactionDto>(), card))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Contact = new ContactDto
                    {
                        Name = "Rose-Marie Grondin",
                        Number = "4183902942",
                        Type = ContactTypes.PHONE
                    },
                    Order = new OrderDto
                    {
                        Header = new HeaderDto
                        {
                            OrderNo = "07C678299"
                        }
                    },
                    Amount = new AmountDto { Balance = 200 }
                });

            _notificationService
                .Setup(x => x.NotifyPaymentReceived(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>(),
                    It.IsAny<string>(), It.IsAny<DateTime>()))
                .ReturnsAsync(false);

            // Act
            var paymentResult = await CreateService().Payment(card, order);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBe(true);
            paymentResult.Message.ShouldBe(string.Empty);
        }

        [Fact]
        public async Task Payment_ShouldReturnSuccessFalseWithEmptyErrorMessage_WhenOgasysPaymentConfirmationReturnsNullOrder()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var orderDto = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            _globalPaymentService
                .Setup(x => x.ProcessAuthorization(card, orderDto))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto(), string.Empty));

            _globalPaymentService
                .Setup(x => x.CaptureTransaction(It.IsAny<TransactionDto>()))
                .Returns(new Tuple<bool, string>(true, string.Empty));

            _ogasysService
                .Setup(x => x.ConfirmPayment(orderDto.EPaymentId, It.IsAny<TransactionDto>(), card))
                .ReturnsAsync((RemotePayOrderDto)null);

            // Act
            var paymentResult = await CreateService().Payment(card, orderDto);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBeFalse();
            paymentResult.Message.ShouldBe(string.Empty);
        }

        [Fact]
        public async Task Payment_ShouldReturnSuccessFalseWithErrorMessage_WhenGlobalPaymentAuthorizationReturnsNullTransaction()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var orderDto = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            _globalPaymentService
                .Setup(x => x.ProcessAuthorization(card, orderDto))
                .Returns(new Tuple<TransactionDto, string>(null, "Error message"));

            // Act
            var paymentResult = await CreateService().Payment(card, orderDto);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBeFalse();
            paymentResult.Message.ShouldBe("Error message");
        }

        [Fact]
        public async Task Payment_ShouldReturnSuccessFalseWithErrorMessage_WhenGlobalPaymentCaptureReturnsFalse()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var orderDto = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            _globalPaymentService
                .Setup(x => x.ProcessAuthorization(card, orderDto))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto(), string.Empty));

            _globalPaymentService
                .Setup(x => x.CaptureTransaction(It.IsAny<TransactionDto>()))
                .Returns(new Tuple<bool, string>(false, "Error message"));

            // Act
            var paymentResult = await CreateService().Payment(card, orderDto);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBeFalse();
            paymentResult.Message.ShouldBe("Error message");
        }

        [Fact]
        public async Task Payment_ShouldReturnSuccessFalseWithErrorMessage_WhenOrderToPayIsInLockedOrdersList()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var orderDto = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            OrderService.LockedOrderNos.Add("07C678299");

            // Act
            var paymentResult = await CreateService().Payment(card, orderDto);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBeFalse();
            paymentResult.Message.ShouldBe("Une transaction est d�j� en cours pour cette commande. Veuillez rafra�chir la page.");
        }

        [Fact]
        public async Task Payment_ShouldReturnUpdatedOrder_WhenPaymentIsCapturedByGlobalPaymentAndConfirmedToOgasys()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var orderDto = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            _globalPaymentService
                .Setup(x => x.ProcessAuthorization(card, orderDto))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto(), string.Empty));

            _globalPaymentService
                .Setup(x => x.CaptureTransaction(It.IsAny<TransactionDto>()))
                .Returns(new Tuple<bool, string>(true, string.Empty));

            _ogasysService
                .Setup(x => x.ConfirmPayment(orderDto.EPaymentId, It.IsAny<TransactionDto>(), card))
                .ReturnsAsync(new RemotePayOrderDto { Order = new OrderDto() });

            // Act
            var paymentResult = await CreateService().Payment(card, orderDto);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBe(true);
            paymentResult.Message.ShouldBe(string.Empty);
        }

        [Fact]
        public async Task Payment_ShouldSendNotificationAndReturnSuccessTrue_WhenNotificationWasSentSuccessfully()
        {
            // Arrange
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            var order = new RemotePayOrderDto
            {
                EPaymentId = "4420674730640090902",
                Contact = new ContactDto(),
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        OrderNo = "07C678299"
                    }
                },
                Amount = new AmountDto { Balance = 200 }
            };

            _globalPaymentService
                .Setup(x => x.ProcessAuthorization(card, order))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto { TimeStamp = DateTime.UtcNow }, string.Empty));

            _globalPaymentService
                .Setup(x => x.CaptureTransaction(It.IsAny<TransactionDto>()))
                .Returns(new Tuple<bool, string>(true, string.Empty));

            _ogasysService
                .Setup(x => x.ConfirmPayment(order.EPaymentId, It.IsAny<TransactionDto>(), card))
                .ReturnsAsync(new RemotePayOrderDto
                {
                    Contact = new ContactDto
                    {
                        Name = "Rose-Marie Grondin",
                        Number = "4183902942",
                        Type = ContactTypes.PHONE
                    },
                    Order = new OrderDto
                    {
                        Header = new HeaderDto
                        {
                            OrderId = "4427845489850068000",
                            OrderNo = "07C678299",
                            OrderDate = DateTime.UtcNow,
                            EntrCode = "45",
                            EntrName = "CD45 - PATRICK MORIN INC.",
                            SubTotal = 150,
                            Total = 200,
                            ShipCustNo = "000015",
                            TaxCode1 = "TPS",
                            TaxCode2 = "TVQ",
                            TaxAmount1 = (decimal)3.6,
                            TaxAmount2 = (decimal)7.18
                        }
                    },
                    Amount = new AmountDto { Balance = 200 }
                });

            _notificationService
                .Setup(x => x.NotifyPaymentReceived(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>(),
                    It.IsAny<string>(), It.IsAny<DateTime>()))
                .ReturnsAsync(true);

            // Act
            var paymentResult = await CreateService().Payment(card, order);

            // Assert
            paymentResult.ShouldNotBeNull();
            paymentResult.Order.ShouldNotBeNull();
            paymentResult.Success.ShouldBe(true);
            paymentResult.Message.ShouldBe(string.Empty);
        }

        [Fact]
        public async Task Refund_ShouldReturnOkResultWithErrorMessage_WhenRefundFailed()
        {
            // Arrange
            var refundItem = new RefundItemDto
            {
                Id = "Id",
                Amount = 30,
                Reference = "ref1",
                ReceiptNumber = "orderno1",
            };

            var refunds = new List<RefundItemDto> {refundItem};
            var contact = new ContactDto {Type = ContactTypes.PHONE, Number = "123 456-7890"};

            _globalPaymentService
                .Setup(x => x.RefundCapturedTransaction(refundItem.Reference, refundItem.ReceiptNumber,
                    refundItem.Amount))
                .Returns(new Tuple<TransactionDto, string>(null, "Error message"));

            _notificationService
                .Setup(x => x.NotifyRefundCompleted(It.IsAny<string>(), It.IsAny<string>(), 30, It.IsAny<string>()))
                .ReturnsAsync(true);

            // Act
            var refundDto = await CreateService().Refund(refunds, contact, "15C880172");

            // Assert
            refundDto.ShouldNotBeNull();
            refundDto.RefundAmountCompleted.ShouldBe(0);
            refundDto.RefundAmountNotCompleted.ShouldBe(30);
            refundDto.RefundNotificationSent.ShouldBeFalse();
            refundDto.RefundResults.ShouldNotBeNull();
            refundDto.RefundResults.ShouldNotBeEmpty();
            refundDto.RefundResults[0].Id.ShouldBe(refundItem.Id);
            refundDto.RefundResults[0].Amount.ShouldBe(refundItem.Amount);
            refundDto.RefundResults[0].Reference.ShouldBe(refundItem.Reference);
            refundDto.RefundResults[0].ReceiptNumber.ShouldBe(refundItem.ReceiptNumber);
            refundDto.RefundResults[0].ErrorCode.ShouldBe(1);
            refundDto.RefundResults[0].ErrorMessage.ShouldBe("Error message");
        }

        [Fact]
        public async Task Refund_ShouldReturnOkResultWithNotificationSentFalse_WhenRefundSucceedsButErrorOccursWhileSendingNotification()
        {
            // Arrange
            var successRefund = new RefundItemDto
            {
                Id = "Id2",
                Amount = 60,
                Reference = "ref2",
                ReceiptNumber = "orderno2",
            };

            var refunds = new List<RefundItemDto> {successRefund};
            var contact = new ContactDto {Type = ContactTypes.PHONE, Number = "123 456-7890"};

            _globalPaymentService
                .Setup(x => x.RefundCapturedTransaction(successRefund.Reference, successRefund.ReceiptNumber,
                    successRefund.Amount))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto(), null));

            _notificationService
                .Setup(x => x.NotifyRefundCompleted(It.IsAny<string>(), It.IsAny<string>(), 60, It.IsAny<string>()))
                .ReturnsAsync(false);

            // Act
            var refundDto = await CreateService().Refund(refunds, contact, "15C880172");

            // Assert
            refundDto.ShouldNotBeNull();
            refundDto.RefundAmountCompleted.ShouldBe(60);
            refundDto.RefundAmountNotCompleted.ShouldBe(0);
            refundDto.RefundNotificationSent.ShouldBeFalse();
            refundDto.RefundResults.ShouldNotBeNull();
            refundDto.RefundResults.ShouldNotBeEmpty();
            refundDto.RefundResults[0].Id.ShouldBe(successRefund.Id);
            refundDto.RefundResults[0].Amount.ShouldBe(successRefund.Amount);
            refundDto.RefundResults[0].Reference.ShouldBe(successRefund.Reference);
            refundDto.RefundResults[0].ReceiptNumber.ShouldBe(successRefund.ReceiptNumber);
            refundDto.RefundResults[0].ErrorCode.ShouldBe(0);
            refundDto.RefundResults[0].ErrorMessage.ShouldBe(string.Empty);
        }

        [Fact]
        public async Task Refund_ShouldSendNotificationAndReturnOkResultWithErrorMessageForFailedRefund_WhenOneRefundFailsAndOneSucceeds()
        {
            // Arrange
            var failedRefund = new RefundItemDto
            {
                Id = "Id",
                Amount = 30,
                Reference = "ref1",
                ReceiptNumber = "orderno1",
            };
            var successRefund = new RefundItemDto
            {

                Id = "Id2",
                Amount = 60,
                Reference = "ref2",
                ReceiptNumber = "orderno2",
            };

            var refunds = new List<RefundItemDto> {failedRefund, successRefund};
            var contact = new ContactDto {Type = ContactTypes.PHONE, Number = "123 456-7890"};

            _globalPaymentService
                .Setup(x => x.RefundCapturedTransaction(failedRefund.Reference, failedRefund.ReceiptNumber,
                    failedRefund.Amount))
                .Returns(new Tuple<TransactionDto, string>(null, "Error message"));

            _globalPaymentService
                .Setup(x => x.RefundCapturedTransaction(successRefund.Reference, successRefund.ReceiptNumber,
                    successRefund.Amount))
                .Returns(new Tuple<TransactionDto, string>(new TransactionDto(), null));

            _notificationService
                .Setup(x => x.NotifyRefundCompleted(It.IsAny<string>(), It.IsAny<string>(), 60, It.IsAny<string>()))
                .ReturnsAsync(true);

            // Act
            var refundDto = await CreateService().Refund(refunds, contact, "15C880172");

            // Assert
            refundDto.ShouldNotBeNull();
            refundDto.RefundAmountCompleted.ShouldBe(60);
            refundDto.RefundAmountNotCompleted.ShouldBe(30);
            refundDto.RefundNotificationSent.ShouldBeTrue();
            refundDto.RefundResults.ShouldNotBeNull();
            refundDto.RefundResults.ShouldNotBeEmpty();
            refundDto.RefundResults[0].Id.ShouldBe(failedRefund.Id);
            refundDto.RefundResults[0].Amount.ShouldBe(failedRefund.Amount);
            refundDto.RefundResults[0].Reference.ShouldBe(failedRefund.Reference);
            refundDto.RefundResults[0].ReceiptNumber.ShouldBe(failedRefund.ReceiptNumber);
            refundDto.RefundResults[0].ErrorCode.ShouldBe(1);
            refundDto.RefundResults[0].ErrorMessage.ShouldBe("Error message");
            refundDto.RefundResults[1].Id.ShouldBe(successRefund.Id);
            refundDto.RefundResults[1].Amount.ShouldBe(successRefund.Amount);
            refundDto.RefundResults[1].Reference.ShouldBe(successRefund.Reference);
            refundDto.RefundResults[1].ReceiptNumber.ShouldBe(successRefund.ReceiptNumber);
            refundDto.RefundResults[1].ErrorCode.ShouldBe(0);
            refundDto.RefundResults[1].ErrorMessage.ShouldBe(string.Empty);
        }

        private OrderService CreateService()
        {
            return new(_logger.Object, _ogasysService.Object, _globalPaymentService.Object,
                _notificationService.Object);
        }
    }
}
