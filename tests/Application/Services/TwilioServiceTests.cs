using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using PatrickMorin.Application.Configuration;
using PatrickMorin.Application.Services;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Application.Services
{
    public class TwilioServiceTests
    {
        private readonly Mock<ILogger<TwilioService>> _logger;
        private readonly Mock<IOptions<TwilioSettings>> _settings;

        public TwilioServiceTests()
        {
            _logger = new Mock<ILogger<TwilioService>>();
            _settings = new Mock<IOptions<TwilioSettings>>();
            _settings.Setup(ap => ap.Value).
                Returns(new TwilioSettings
                {
                    FromNumber = "+15813331600"
                });
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData("44 444-4444")]
        [InlineData("(444-444-4444")]
        public void SendTextMessage_ShouldReturnFalse_WhenPhoneNumberDoesNotMatchRegex(string toPhoneNumber)
        {
            // Arrange
            var twilioService = CreateService();

            // Act
            var success = twilioService.SendTextMessage(toPhoneNumber, "");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public void SendTextMessage_ShouldReturnFalse_WhenCreateMessageThrowsException()
        {
            // Arrange
            var twilioService = CreateService();

            // Act
            var success = twilioService.SendTextMessage("418 111-2222", "");

            // Assert
            success.ShouldBeFalse();
        }

        private TwilioService CreateService()
        {
            return new TwilioService(_settings.Object, _logger.Object);
        }
    }
}
