using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.Core.Models;
using Moq;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Mailing;
using PatrickMorin.Application.Mailing.Templates;
using PatrickMorin.Application.Services;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Application.Services
{
    public class NotificationServiceTests
    {
        private readonly Mock<IEmailFactory> _emailFactory;
        private readonly Mock<ITwilioService> _twilioService;

        public NotificationServiceTests()
        {
            _emailFactory = new Mock<IEmailFactory>();
            _twilioService = new Mock<ITwilioService>();
        }
        
        [Fact]
        public async Task NotifyOrderReceived_ShouldReturnFalse_WhenContactTypeIsEmailAndEmailSendFails()
        {
            // Arrange
            var service = CreateService();
            var email = new Mock<Email>();
            email
                .Setup(x => x.SendAsync(null))
                .ReturnsAsync(new SendResponse { ErrorMessages = new List<string> { "Error" } });

            _emailFactory
                .Setup(x => x.Prepare(It.IsAny<OrderReceivedEmail>()))
                .Returns(email.Object);

            // Act
            var success = await service
                .NotifyOrderReceived(ContactTypes.EMAIL, "rmgrondin@spektrummedia.com", "orderNo", "orderUrl");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyOrderReceived_ShouldReturnFalse_WhenContactTypeIsNotHandled()
        {
            // Arrange
            var service = CreateService();

            // Act
            var success = await service
                .NotifyOrderReceived("othertype", "rmgrondin@spektrummedia.com", "orderNo", "orderUrl");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyOrderReceived_ShouldReturnFalse_WhenContactTypeIsPhoneAndTextMessageSendFails()
        {
            // Arrange
            var service = CreateService();

            // Act
            var success = await service
                .NotifyOrderReceived(ContactTypes.PHONE, "rmgrondin@spektrummedia.com", "orderNo", "orderUrl");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyOrderReceived_ShouldReturnTrue_WhenContactTypeIsEmailAndEmailSendSucceeds()
        {
            // Arrange
            var service = CreateService();
            _emailFactory
                .Setup(x => x.Prepare(It.IsAny<OrderReceivedEmail>()).SendAsync(null))
                .ReturnsAsync(new SendResponse());

            // Act
            var success = await service
                .NotifyOrderReceived(ContactTypes.EMAIL, "rmgrondin@spektrummedia.com", "orderNo", "orderUrl");

            // Assert
            success.ShouldBeTrue();
        }

        [Fact]
        public async Task NotifyOrderReceived_ShouldReturnTrue_WhenContactTypeIsPhoneAndTextMessageSendSucceeds()
        {
            // Arrange
            var service = CreateService();
            _twilioService
                .Setup(x => x.SendTextMessage(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            // Act
            var success = await service
                .NotifyOrderReceived(ContactTypes.PHONE, "rmgrondin@spektrummedia.com", "orderNo", "orderUrl");

            // Assert
            success.ShouldBeTrue();
        }

        [Fact]
        public async Task NotifyPaymentReceived_ShouldReturnFalse_WhenContactTypeIsEmailAndEmailSendFails()
        {
            // Arrange
            var service = CreateService();
            var email = new Mock<Email>();
            email
                .Setup(x => x.SendAsync(null))
                .ReturnsAsync(new SendResponse { ErrorMessages = new List<string> { "Error" } });

            _emailFactory
                .Setup(x => x.Prepare(It.IsAny<PaymentReceivedEmail>()))
                .Returns(email.Object);

            // Act
            var success = await service
                .NotifyPaymentReceived(ContactTypes.EMAIL, "rmgrondin@spektrummedia.com", 100, "orderUrl", DateTime.UtcNow);

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyPaymentReceived_ShouldReturnFalse_WhenContactTypeIsNotHandled()
        {
            // Arrange
            var service = CreateService();

            // Act
            var success = await service
                .NotifyPaymentReceived("othertype", "rmgrondin@spektrummedia.com", 100, "orderUrl", DateTime.UtcNow);

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyPaymentReceived_ShouldReturnFalse_WhenContactTypeIsPhoneAndTextMessageSendFails()
        {
            // Arrange
            var service = CreateService();

            // Act
            var success = await service
                .NotifyPaymentReceived(ContactTypes.PHONE, "rmgrondin@spektrummedia.com", 100, "orderUrl", DateTime.UtcNow);

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyPaymentReceived_ShouldReturnTrue_WhenContactTypeIsEmailAndEmailSendSucceeds()
        {
            // Arrange
            var service = CreateService();

            _emailFactory
                .Setup(x => x.Prepare(It.IsAny<PaymentReceivedEmail>()).SendAsync(null))
                .ReturnsAsync(new SendResponse());

            // Act
            var success = await service
                .NotifyPaymentReceived(ContactTypes.EMAIL, "rmgrondin@spektrummedia.com", 100, "orderUrl", DateTime.UtcNow);

            // Assert
            success.ShouldBeTrue();
        }

        [Fact]
        public async Task NotifyPaymentReceived_ShouldReturnTrue_WhenContactTypeIsPhoneAndTextMessageSendSucceeds()
        {
            // Arrange
            var service = CreateService();
            _twilioService
                .Setup(x => x.SendTextMessage(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            // Act
            var success = await service
                .NotifyPaymentReceived(ContactTypes.PHONE, "rmgrondin@spektrummedia.com", 100, "orderUrl", DateTime.UtcNow);

            // Assert
            success.ShouldBeTrue();
        }

        [Fact]
        public async Task NotifyRefundCompleted_ShouldReturnFalse_WhenContactTypeIsEmailAndEmailSendFails()
        {
            // Arrange
            var service = CreateService();
            var email = new Mock<Email>();
            email
                .Setup(x => x.SendAsync(null))
                .ReturnsAsync(new SendResponse { ErrorMessages = new List<string> { "Error" } });

            _emailFactory
                .Setup(x => x.Prepare(It.IsAny<RefundCompletedEmail>()))
                .Returns(email.Object);

            // Act
            var success = await service
                .NotifyRefundCompleted(ContactTypes.EMAIL, "rmgrondin@spektrummedia.com", 100, "orderUrl");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyRefundCompleted_ShouldReturnFalse_WhenContactTypeIsNotHandled()
        {
            // Arrange
            var service = CreateService();

            // Act
            var success = await service
                .NotifyRefundCompleted("othertype", "rmgrondin@spektrummedia.com", 100, "orderUrl");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyRefundCompleted_ShouldReturnFalse_WhenContactTypeIsPhoneAndTextMessageSendFails()
        {
            // Arrange
            var service = CreateService();

            // Act
            var success = await service
                .NotifyRefundCompleted(ContactTypes.PHONE, "rmgrondin@spektrummedia.com", 100, "orderUrl");

            // Assert
            success.ShouldBeFalse();
        }

        [Fact]
        public async Task NotifyRefundCompleted_ShouldReturnTrue_WhenContactTypeIsEmailAndEmailSendSucceeds()
        {
            // Arrange
            var service = CreateService();
            _emailFactory
                .Setup(x => x.Prepare(It.IsAny<RefundCompletedEmail>()).SendAsync(null))
                .ReturnsAsync(new SendResponse());

            // Act
            var success = await service
                .NotifyRefundCompleted(ContactTypes.EMAIL, "rmgrondin@spektrummedia.com", 100, "orderUrl");

            // Assert
            success.ShouldBeTrue();
        }

        [Fact]
        public async Task NotifyRefundCompleted_ShouldReturnTrue_WhenContactTypeIsPhoneAndTextMessageSendSucceeds()
        {
            // Arrange
            var service = CreateService();
            _twilioService
                .Setup(x => x.SendTextMessage(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            // Act
            var success = await service
                .NotifyRefundCompleted(ContactTypes.PHONE, "rmgrondin@spektrummedia.com", 100, "orderUrl");

            // Assert
            success.ShouldBeTrue();
        }

        private NotificationService CreateService()
        {
            return new(_emailFactory.Object, _twilioService.Object);
        }
    }
}
