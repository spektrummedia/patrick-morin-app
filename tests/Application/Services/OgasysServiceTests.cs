using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using PatrickMorin.Application.Configuration;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Services;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Application.Services
{
    public class OgasysServiceTests
    {
        private readonly Mock<ILogger<OgasysService>> _logger;
        private readonly Mock<IOptions<OgasysSettings>> _settings;
        private readonly Mock<IHttpRequestService> _httpRequestService;

        public OgasysServiceTests()
        {
            _logger = new Mock<ILogger<OgasysService>>();
            _httpRequestService = new Mock<IHttpRequestService>();
            _settings = new Mock<IOptions<OgasysSettings>>();
            _settings.Setup(ap => ap.Value).
                Returns(new OgasysSettings());
        }

        [Fact]
        public async Task ConfirmPayment_ShouldReturnNull_WhenCardIsNull()
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmPayment("4420674730640090902", new TransactionDto(), null);

            // Assert
            order.ShouldBeNull();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public async Task ConfirmPayment_ShouldReturnNull_WhenEPaymentIdIsNullEmptyOrWhitespace(string ePaymentId)
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmPayment(ePaymentId, new TransactionDto(), new CardDto());

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task ConfirmPayment_ShouldReturnNull_WhenResponseStatusCodeIsNotOk()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var transaction = new TransactionDto
            {
                AuthorizedAmount = 229,
                PaymentsReference = "9999",
                OrderId = "8888",
                CardType = "Visa"
            };
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.BadRequest));

            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmPayment(ePaymentId, transaction, card);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task ConfirmPayment_ShouldReturnNull_WhenTransactionIsNull()
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmPayment("4420674730640090902", null, new CardDto());

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task ConfirmPayment_ShouldReturnOrder_WhenResponseStatusCodeIsOk()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var transaction = new TransactionDto
            {
                AuthorizedAmount = 229,
                PaymentsReference = "9999",
                OrderId = "8888",
                CardType = "Visa"
            };
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new RemotePayOrderDto()))
                });

            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmPayment(ePaymentId, transaction, card);

            // Assert
            order.ShouldNotBeNull();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public async Task ConfirmRefund_ShouldReturnNull_WhenCardMaskIsNullEmptyOrWhitespace(string cardMask)
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmRefund("4420674730640090902", new TransactionDto(), cardMask);

            // Assert
            order.ShouldBeNull();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public async Task ConfirmRefund_ShouldReturnNull_WhenEPaymentIdIsNullEmptyOrWhitespace(string ePaymentId)
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmRefund(ePaymentId, new TransactionDto(), "XXXX XXXX XXXX 4415");

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task ConfirmRefund_ShouldReturnNull_WhenResponseStatusCodeIsNotOk()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var cardMask = "XXXX XXXX XXXX 4415";
            var transaction = new TransactionDto
            {
                AuthorizedAmount = 229,
                PaymentsReference = "9999",
                OrderId = "8888",
                CardType = "Visa"
            };

            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.BadRequest));

            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmRefund(ePaymentId, transaction, cardMask);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task ConfirmRefund_ShouldReturnNull_WhenTransactionIsNull()
        {
            // Arrange
            var cardMask = "XXXX XXXX XXXX 4415";
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmRefund("4420674730640090902", null, cardMask);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task ConfirmRefund_ShouldReturnOrder_WhenResponseStatusCodeIsOk()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            var cardMask = "XXXX XXXX XXXX 4415";
            var transaction = new TransactionDto
            {
                AuthorizedAmount = 229,
                PaymentsReference = "9999",
                OrderId = "8888",
                CardType = "Visa"
            };
            var card = new CardDto
            {
                CardholderName = "Rose-Marie Grondin",
                CardNumber = "1111 2222 3333 4444",
                ExpiryYear = DateTime.Today.Year + 2,
                ExpiryMonth = 12,
                SecurityCode = "999"
            };

            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new RemotePayOrderDto()))
                });

            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.ConfirmRefund(ePaymentId, transaction, cardMask);

            // Assert
            order.ShouldNotBeNull();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task GetRemotePayOrderByOrderNo_ShouldReturnNull_WhenOrderNoIsNullEmptyOrWhitespace(string orderNo)
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.GetRemotePayOrderByOrderNo(orderNo);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task GetRemotePayOrderByOrderNo_ShouldReturnNull_WhenOgasysRequestReturnsAStatusCodeThatIsNotOk()
        {
            // Arrange
            var orderNo = "07C678299";
            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.NotFound));
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.GetRemotePayOrderByOrderNo(orderNo);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task GetRemotePayOrderByOrderNo_ShouldReturnOrder_WhenOgasysRequestReturnsStatusCodeOk()
        {
            // Arrange
            var orderNo = "07C678299";
            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new RemotePayOrderDto()))
                });
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.GetRemotePayOrderByOrderNo(orderNo);

            // Assert
            order.ShouldNotBeNull();
        }


        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task GetRemotePayOrder_ShouldReturnNull_WhenEPaymentIdIsNullEmptyOrWhitespace(string ePaymentId)
        {
            // Arrange
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.GetRemotePayOrder(ePaymentId);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task GetRemotePayOrder_ShouldReturnNull_WhenOgasysRequestReturnsAStatusCodeThatIsNotOk()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.NotFound));
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.GetRemotePayOrder(ePaymentId);

            // Assert
            order.ShouldBeNull();
        }

        [Fact]
        public async Task GetRemotePayOrder_ShouldReturnOrder_WhenOgasysRequestReturnsStatusCodeOk()
        {
            // Arrange
            var ePaymentId = "4420674730640090902";
            _httpRequestService
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new RemotePayOrderDto()))
                });
            var ogasysService = CreateService();

            // Act
            var order = await ogasysService.GetRemotePayOrder(ePaymentId);

            // Assert
            order.ShouldNotBeNull();
        }

        private OgasysService CreateService()
        {
            return new(_settings.Object, _logger.Object, _httpRequestService.Object);
        }
    }
}
