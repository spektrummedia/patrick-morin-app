using System;
using GlobalPayments.Api.Entities;
using GlobalPayments.Api.PaymentMethods;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using PatrickMorin.Application.Configuration;
using PatrickMorin.Application.Dtos.GlobalPayments;
using PatrickMorin.Application.Dtos.Ogasys;
using PatrickMorin.Application.Enums;
using PatrickMorin.Application.Services;
using Shouldly;
using Xunit;

namespace PatrickMorin.Tests.Application.Services
{
    public class GlobalPaymentServiceTests
    {
        private readonly Mock<ILogger<GlobalPaymentService>> _logger;
        private readonly Mock<IOptions<GlobalPaymentSettings>> _settings;
        private readonly Mock<IGlobalPaymentTransactionService> _transactionService;

        // GlobalPayments test cards 
        private const string VALID_CARD = "4263970000005262";
        private const string CARD_BANK_DECLINED = "4000120000001154";
        private const string CARD_REFERAL_A = "4000160000004147";
        private const string CARD_REFERAL_B = "4000130000001724";
        private const string CARD_COMMUNICATION_ERROR = "4009830000001985";

        public GlobalPaymentServiceTests()
        {
            _logger = new Mock<ILogger<GlobalPaymentService>>();
            _settings = new Mock<IOptions<GlobalPaymentSettings>>();
            _transactionService = new Mock<IGlobalPaymentTransactionService>();
            _settings.Setup(ap => ap.Value).Returns(new GlobalPaymentSettings
            {
                Account = "Account",
                Currency = "CAD",
                MerchantId = "MerchantId",
                RebatePassword = "RebatePassword",
                SharedSecret = "SharedSecret",
                Url = "Url"
            });
        }

        [Fact]
        public void CaptureTransaction_ShouldReturnTupleFalseAndErrorOccuredMessage_WhenTransactionIdAndOrderIdAreNull()
        {
            // Arrange
            var service = CreateService();
            _transactionService
                .Setup(x => x.CaptureTransaction(It.IsAny<Transaction>(), It.IsAny<decimal>()))
                .Throws(new ApiException());

            // Act
            var tuple = service.CaptureTransaction(new TransactionDto());

            // Assert
            tuple.Item1.ShouldBeFalse();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Fact]
        public void CaptureTransaction_ShouldReturnTupleFalseAndErrorOccuredMessage_WhenTransactionIsNull()
        {
            // Arrange
            var service = CreateService();

            // Act
            var tuple = service.CaptureTransaction(null);

            // Assert
            tuple.Item1.ShouldBeFalse();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Fact]
        public void CaptureTransaction_ShouldReturnTupleTrueAndEmptyMessage_WhenResponseCodeIsNotZeroZero()
        {
            // Arrange
            var service = CreateService();
            _transactionService
                .Setup(x => x.CaptureTransaction(It.IsAny<Transaction>(), It.IsAny<decimal>()))
                .Returns(new Transaction
                {
                    ResponseCode = "101"
                });

            // Act
            var tuple = service.CaptureTransaction(new TransactionDto());

            // Assert
            tuple.Item1.ShouldBeFalse();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Fact]
        public void CaptureTransaction_ShouldReturnTupleTrueAndEmptyMessage_WhenTransactionServiceReturnsResponseCodeZeroZero()
        {
            // Arrange
            var service = CreateService();
            _transactionService
                .Setup(x => x.CaptureTransaction(It.IsAny<Transaction>(), It.IsAny<decimal>()))
                .Returns(new Transaction
                {
                    ResponseCode = "00"
                });

            // Act
            var tuple = service.CaptureTransaction(new TransactionDto());

            // Assert
            tuple.Item1.ShouldBeTrue();
            tuple.Item2.ShouldBeNullOrEmpty();
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithAuthorizationErrorMessage_WhenCardAuthorizationFails()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Throws(new ApiException(GlobalPaymentErrors.REF_B_AUTHORIZATION_ERROR));

            var card = new CardDto
            {
                CardNumber = CARD_REFERAL_B,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year + 2,
                SecurityCode = "999"
            };

            var remoteOrderDto = new RemotePayOrderDto
            {
                Amount = new AmountDto {Balance = 250},
                Order = new OrderDto { Header = new HeaderDto { ShipCustNo = "000002" } }
            };
            
            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Carte refus�e par la banque.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithBankDeclinedErrorMessage_WhenBankDeclinedCard()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Throws(new ApiException(GlobalPaymentErrors.BANK_DECLINED));

            var card = new CardDto
            {
                CardNumber = CARD_BANK_DECLINED,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year + 2,
                SecurityCode = "999"
            };

            var remoteOrderDto = new RemotePayOrderDto
            {
                Amount = new AmountDto { Balance = 250 },
                Order = new OrderDto { Header = new HeaderDto { ShipCustNo = "000002" } }
            };

            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Carte ou transaction refus�e par la banque. Validez que les informations entr�es " +
                                 "sont exactes et que les fonds sont suffisants.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithCustomMessage_WhenCardInfoIsInvalid()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Throws(new ApiException(GlobalPaymentErrors.CREDIT_CARD + " - La date d'expiration est invalide."));

            var card = new CardDto
            {
                CardNumber = VALID_CARD,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 01,
                ExpiryYear = DateTime.Today.Year - 1,
                SecurityCode = "999"
            };

            var remoteOrderDto = new RemotePayOrderDto
            {
                Amount = new AmountDto { Balance = 250 },
                Order = new OrderDto { Header = new HeaderDto { ShipCustNo = "000002" } }
            };

            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.Trim().ShouldBe("La date d'expiration est invalide.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithErrorOccuredMessage_WhenAnyOtherErrorOccurs()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Throws(new ApiException());

            var card = new CardDto
            {
                CardNumber = CARD_COMMUNICATION_ERROR,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year + 2,
                SecurityCode = "999"
            };

            var remoteOrderDto = new RemotePayOrderDto
            {
                Amount = new AmountDto { Balance = 250 },
                Order = new OrderDto { Header = new HeaderDto { ShipCustNo = "000002" } }
            };

            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-20)]
        public void ProcessAuthorization_ShouldReturnTupleWithNullValues_WhenAmountIsLessOrEqualToZero(decimal amount)
        {
            // Arrange
            var service = CreateService();
            var remoteOrderDto = new RemotePayOrderDto { Amount = new AmountDto { Balance = amount } };

            // Act
            var tuple = service.ProcessAuthorization(new CardDto(), remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Cette commande a d�j� �t� pay�e.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithNullValues_WhenCardDtoIsNull()
        {
            // Arrange
            var service = CreateService();
            var remoteOrderDto = new RemotePayOrderDto { Amount = new AmountDto { Balance = 250 } };

            // Act
            var tuple = service.ProcessAuthorization(null, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithReportedStolenErrorMessage_WhenCardIsReportedStolen()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Throws(new ApiException(GlobalPaymentErrors.REF_A_REPORTED_STOLEN));

            var card = new CardDto
            {
                CardNumber = CARD_REFERAL_A,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year + 2,
                SecurityCode = "999"
            };
            var remoteOrderDto = new RemotePayOrderDto 
            {
                Amount = new AmountDto { Balance = 250 },
                Order = new OrderDto { Header = new HeaderDto { ShipCustNo = "000002" } }
            };

            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Carte refus�e par la banque.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithTransaction_WhenResponseCodeIsNotZeroZero()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Returns(new Transaction
                {
                    ResponseCode = "101"
                });

            var card = new CardDto
            {
                CardNumber = VALID_CARD,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year + 2,
                SecurityCode = "999"
            };

            var remoteOrderDto = new RemotePayOrderDto
            {
                Amount = new AmountDto { Balance = 250 },
                Order = new OrderDto { Header = new HeaderDto { ShipCustNo = "000002" } }
            };

            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le paiement.");
        }

        [Fact]
        public void ProcessAuthorization_ShouldReturnTupleWithTransaction_WhenResponseCodeIsZeroZero()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.AuthorizeTransaction(It.IsAny<CreditCardData>(), It.IsAny<TransactionDataDto>()))
                .Returns(new Transaction
                {
                    ResponseCode = "00",
                    Timestamp = "20210315164455"
                });

            var card = new CardDto
            {
                CardNumber = VALID_CARD,
                CardholderName = "Rose-Marie Grondin",
                ExpiryMonth = 12,
                ExpiryYear = DateTime.Today.Year + 2,
                SecurityCode = "999"
            };
            var remoteOrderDto = new RemotePayOrderDto
            {
                Amount = new AmountDto { Balance = 250 },
                Contact = new ContactDto
                {
                    Name = "Rose-Marie Grondin",
                    Number = "111 222-33440",
                    Type = ContactTypes.PHONE
                },
                Order = new OrderDto
                {
                    Header = new HeaderDto
                    {
                        ShipAddress = new AddressDto
                        {
                            Address1 = "111, rue Bouvier",
                            City = "Qu�bec",
                            Country = "CA",
                            Postcode = "G6G 3G5",
                            Province = "QC"
                        }
                    }
                }
            };

            // Act
            var tuple = service.ProcessAuthorization(card, remoteOrderDto);

            // Assert
            tuple.Item1.ShouldNotBeNull();
            tuple.Item2.ShouldBeNullOrEmpty();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void RefundCapturedTransaction_ShouldReturnTupleWithErrorOccuredMessage_WhenOrderIdIsNullEmptyOrWhiteSpace(string orderId)
        {
            // Arrange
            var service = CreateService();

            // Act
            var tuple = service.RefundCapturedTransaction("16152199838537636", orderId, 200);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le remboursement.");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void RefundCapturedTransaction_ShouldReturnTupleWithErrorOccuredMessage_WhenPaymentsReferenceIsNullEmptyOrWhiteSpace(string paymentsReference)
        {
            // Arrange
            var service = CreateService();

            // Act
            var tuple = service.RefundCapturedTransaction(paymentsReference, "YEuCLyBkPU2QtulDuzRsCw", 200);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le remboursement.");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-400)]
        public void RefundCapturedTransaction_ShouldReturnTupleWithErrorOccuredMessage_WhenRefundAmountIsEqualOrLessThanZero(decimal refundAmount)
        {
            // Arrange
            var service = CreateService();

            // Act
            var tuple = service.RefundCapturedTransaction("16152199838537636", "YEuCLyBkPU2QtulDuzRsCw", refundAmount);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le remboursement.");
        }

        [Fact]
        public void RefundCapturedTransaction_ShouldReturnTupleWithErrorOccuredMessage_WhenResponseCodeIsNotZeroZero()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.RefundCapturedTransaction(It.IsAny<Transaction>(), It.IsAny<decimal>(), It.IsAny<string>()))
                .Returns(new Transaction { ResponseCode = "101"});

            // Act
            var tuple = service.RefundCapturedTransaction("16152199838537636", "YEuCLyBkPU2QtulDuzRsCw", 200);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le remboursement.");
        }

        [Fact]
        public void RefundCapturedTransaction_ShouldReturnTupleWithErrorOccuredMessage_WhenTransactionServiceThrowsError()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.RefundCapturedTransaction(It.IsAny<Transaction>(), It.IsAny<decimal>(), It.IsAny<string>()))
                .Throws(new ApiException());

            // Act
            var tuple = service.RefundCapturedTransaction("16152199838537636", "YEuCLyBkPU2QtulDuzRsCw", 200);

            // Assert
            tuple.Item1.ShouldBeNull();
            tuple.Item2.ShouldBe("Une erreur est survenue pendant le remboursement.");
        }

        [Fact]
        public void RefundCapturedTransaction_ShouldReturnTupleWithTransaction_WhenResponseCodeIsZeroZero()
        {
            // Arrange
            var service = CreateService();

            _transactionService
                .Setup(x => x.RefundCapturedTransaction(It.IsAny<Transaction>(), It.IsAny<decimal>(), It.IsAny<string>()))
                .Returns(new Transaction { ResponseCode = "00", Timestamp = "20210315164455" });

            // Act
            var tuple = service.RefundCapturedTransaction("16152199838537636", "YEuCLyBkPU2QtulDuzRsCw", 200);

            // Assert
            tuple.Item1.ShouldNotBeNull();
            tuple.Item2.ShouldBeEmpty();
        }

        private GlobalPaymentService CreateService()
        {
            return new(_settings.Object, _logger.Object, _transactionService.Object);
        }
    }
}
